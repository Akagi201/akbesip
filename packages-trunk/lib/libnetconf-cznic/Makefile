#
# Copyright (C) 2013 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=libnetconf-cznic
PKG_VERSION:=0.5.0
PKG_RELEASE:=1
PKG_REV:=d0de34a63d9336b296b12dca07dd65b374a9413f
PKG_INSTALL:=1

PKG_SOURCE:=$(PKG_NAME)-$(PKG_VERSION).tar.gz
PKG_SOURCE_URL:=https://gitlab.labs.nic.cz/router/libnetconf.git
PKG_SOURCE_PROTO:=git
PKG_SOURCE_VERSION:=$(PKG_REV)
PKG_SOURCE_SUBDIR:=$(PKG_NAME)

PKG_BUILD_DIR=$(BUILD_DIR)/$(PKG_NAME)

include $(INCLUDE_DIR)/package.mk
include $(INCLUDE_DIR)/nls.mk

define Package/libnetconf-cznic
  SECTION:=libs
  CATEGORY:=Libraries
  TITLE:=NETCONF library
  URL:=https://gitlab.labs.nic.cz/router/libnetconf
  DEPENDS:=+libssh2 +libxml2 +libdbus +librt
endef

define Package/libnetconf-cznic/Default/description
libnetconf is the NETCONF library in C intended for building NETCONF clients and servers.
libnetconf provides basic functions to connect NETCONF client and server to each other via
SSH, to send and receive NETCONF messages and to store and work with the configuration data 
in a datastore.
endef

CONFIGURE_ARGS+= \
	--prefix=/usr/ \
	--libdir=/usr/lib/ \
	--includedir=/usr/include \
	--with-streamspath=/var/libnetconf/ \
	--enable-examples

define Build/Compile
	$(MAKE) $(MAKE_FLAGS) -C $(PKG_BUILD_DIR) all
endef

define Build/Install
	$(MAKE) $(MAKE_FLAGS) -C $(PKG_BUILD_DIR) install DESTDIR=$(PKG_INSTALL_DIR); true
endef

define Build/InstallDev
	$(INSTALL_DIR) $(1)/usr/include
	$(CP) \
		$(PKG_INSTALL_DIR)/usr/include/*h \
		$(1)/usr/include/
	$(INSTALL_DIR) $(1)/usr/include/libnetconf
	$(CP) \
		$(PKG_INSTALL_DIR)/usr/include/libnetconf/*h \
		$(1)/usr/include/libnetconf/
	$(INSTALL_DIR) $(1)/usr/lib
	$(INSTALL_BIN) \
		$(PKG_INSTALL_DIR)/usr/lib/libnetconf* \
		$(1)/usr/lib/
	$(INSTALL_DIR) $(1)/usr/lib/pkgconfig
	$(CP) \
		$(PKG_INSTALL_DIR)/usr/lib/pkgconfig/libnetconf.pc \
		$(1)/usr/lib/pkgconfig/
	$(INSTALL_DIR) $(1)/var/libnetconf
endef

define Package/libnetconf-cznic/install
	$(INSTALL_DIR) $(1)/usr/lib
	$(INSTALL_BIN) \
		$(PKG_INSTALL_DIR)/usr/lib/libnetconf.so* \
		$(1)/usr/lib/
endef

$(eval $(call BuildPackage,libnetconf-cznic))
