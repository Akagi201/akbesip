#!/usr/bin/env python
# -*- coding: utf-8 -*-

from config.config import Config
import networkx as nx
from zabbix_api import ZabbixAPI
from pprint import *


zapi = ZabbixAPI(server=Config.zabbix_url, path="", log_level=1)
zapi.login(Config.zabbix_login, Config.zabbix_password)

width = 1024
height = 756
mapname = "topology"


# === CREATE INITIAL GRAPH ===
G=nx.Graph()

fields = ["host", "name", "hostid","status"]
probes = zapi.host.get({ "output":fields,"filter": {}})

edges = {}   
for p in probes:  
    edges[p['host']] = {}
    items = zapi.item.get({ "output":['name','lastvalue'],"filter": {'hostid':p['hostid']}})
    for item in items:
        parts = item['name'].split('-');
        if parts[0] == 'pesq' and len(parts) == 2 :
            G.add_edge(p['host'], parts[1], value = item['lastvalue'])
            G.node[p['host']]["hostname"] = p['name']
            edges[p['host']][parts[1]] = item['lastvalue']
   
print "----- EDGES -------"

edges_labeled = {};
for e in edges:
    edges_labeled[e] = {};
    for e2 in edges[e]:
        if ( e in edges_labeled and e2 in edges_labeled[e] ) or ( e2 in edges_labeled and e in edges_labeled[e2]):
            label = ''
        else:
            label = e+" > "+e2+":"+edges[e][e2]+" "
            label += ", "+e2+" > "+e+":"+edges[e2][e]
            edges_labeled[e][e2] = label
         
pprint(edges_labeled)
pos = nx.graphviz_layout(G)

pprint(edges_labeled)
# Find maximum coordinate values of the layout to scale it better to the desired output size
# The origin is different between Zabbix (top left) and Graphviz (bottom left)
# Join the temporary selementid necessary for links and the coordinates to the node data

poslist=list(pos.values())
maxpos=map(max, zip(*poslist))
    
for host, coordinates in pos.iteritems():
   pos[host] = [int(coordinates[0]*width/maxpos[0]*0.95-coordinates[0]*0.1), int((height-coordinates[1]*height/maxpos[1])*0.95+coordinates[1]*0.1)]
nx.set_node_attributes(G,'coordinates',pos)

selementids = dict(enumerate(G.nodes_iter(), start=1))
selementids = dict((v,k) for k,v in selementids.iteritems())
nx.set_node_attributes(G,'selementid',selementids)

map_params = {
    "name": mapname,
    "label_type": 0,
    "width": width,
    "height": height
}
element_params=[]
link_params=[]

for node, data in G.nodes_iter(data=True):
    element_params.append({
        "elementtype": 4,
        "elementid": 0,
        "selementid": data['selementid'],
        "iconid_off": "74",
        "use_iconmap": 0,
        "label": node,
        "x": data['coordinates'][0],
        "y": data['coordinates'][1],
        })

# Iterate through edges to create the Zabbix links, based on selementids
nodenum = nx.get_node_attributes(G,'selementid')

for nodea, nodeb in G.edges_iter():
    print nodea
    print nodeb
    link_params.append({
        "selementid1": nodenum[nodea],
        "selementid2": nodenum[nodeb],
        "label": edges_labeled[nodea][nodeb]
        })

# Join the map data together
map_params["selements"] = element_params
map_params["links"] = link_params
    

# Get rid of an existing map of that name and create a new one
del_mapid = zapi.map.get({"filter": {"name": mapname}})
if del_mapid:
    zapi.map.delete([del_mapid[0]['sysmapid']])

map=zapi.map.create(map_params)
        
  
    
