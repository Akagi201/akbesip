#!/usr/bin/env python
# -*- coding: utf-8 -*-


from libs.zabbix_json_class     import ZabbixJson
from libs.acl_class             import AsteriskCallList
from libs.asterisk_ami_class    import AstConnection
from pprint                     import pprint
import uuid


if __name__ == "__main__":
    local_identity      = str(hex(uuid.getnode()).lstrip("0x").rstrip("L")).zfill(12)
    zjs                 = ZabbixJson()
    list_of_hosts       = sorted([host['host'] for host in zjs.get_hosts(['host'])])
    acl                 = AsteriskCallList(list_of_hosts, local_identity)
    local_acl           = acl.acl     # list of tuples [(origin, destination), ....]
    asc                 = AstConnection()

    for item in local_acl:
        asc.connect()
        asc.send_action('login')
        result = asc.read_response('Success')
        if result:
            asc.send_action(
                            action      = 'Originate',
                            attributes  = {
                                            'Channel' : 'Local/%s@local-loop' % item[1],
                                            'Context' : 'to-probe',
                                            'Exten'   : '%s' % item[1],
                                            'Priority': '1',
                                            'Timeout' : '30000'
                                            }
                            )
            if asc.read_response('Success'):
                print "Call Attempt Successfully Passed to Asterisk"
            else:
                print "Call Attempt Failed"

        else:
            print "Asterisk login failed"




