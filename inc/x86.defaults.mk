
# Defaults for x86 targets. Can be included directly from target.

# Sysupgrade is disc
OWRT_IMG_SYSUPGRADE_NAME=$(OWRT_IMG_DISK_NAME)

OWRT_CONFIG_SET += TARGET_ROOTFS_SQUASHFS=y TARGET_IMAGES_PAD=y GRUB_IMAGES=y TARGET_ROOTFS_PARTNAME=\"/dev/sda2\"

EMBEDED_MODULES += SATA_AHCI SATA_INIC162X SATA_SIL24 ATA_SFF NETCONSOLE ACPI MTD SQUASHFS MTD_BLOCK2MTD \
		USB USB_SUPPORT USB_KBD USB_MOUSE USB_OHCI_HCD USB_EHCI_HCD \
		HID HIDRAW HID_GENERIC INPUT INPUT_EVDEV

BESIP_PACKAGES += kmod-usb-net=y \
		unbound-host=y ppp=y kmod-tun=y \
		acpid=y kmod-acpi-button=y kmod-button-hotplug=y \
		block-mount=y swap-utils=y fdisk=y e2fsprogs=y resize2fs=y tune2fs=y kmod-fs-btrfs=y btrfs-progs=y kmod-fs-ext4=y kmod-crypto-core=y \
		atftp=y

OWRT_CONFIG_UNSET += TARGET_ROOTFS_JFFS2 TARGET_ROOTFS_EXT2FS TARGET_ROOTFS_EXT4FS TARGET_ROOTFS_TARGZ

KVM_BOOT_FILE="tftp://10.0.2.2/uci{fd}"
KVM_HOSTNAME="besip"
KVM_DNSSEARCH="besipnet.local"

