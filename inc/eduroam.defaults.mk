
BESIPPKG=n
BESIPPKG_LITE=n
BESIPPKG_HUGE=n
BESIPPKG_DEV=n

BESIP_NODEFAULTS=packages uciprov files

BESIP_PACKAGES= luci=m uciprov=y kmod-ipv6=y hostapd=y \
  zabbix-agentd=y zabbix-extra-mac80211=y zabbix-extra-network=y zabbix-extra-wifi=y \
  atftp=y unbound-host=m ppp=m kmod-pppoe=m ppp-mod-pppoe=m kmod-ppp=m kmod-tun=y \
  wpad=y wpad-mini=n haveged=y lldpd=y cdp-tools=m scdp=y \
  openvpn-openssl=m tcpdump=m iftop=m  ip=y

OWRT_ADDED_FILES += $(call AddDirFiles,eduroamap) etc/uci-defaults/besip

OWRT_CONFIG_SET += UCIPROV_REBOOT_AFTER_CFG_APPLY=y UCIPROV_AUTOSTART=y UCIPROV_WAITFOROVERLAY="180" UCIPROV_SU=y UCIPROV_TGZ=y UCIPROV_USE_STATIC=y UCIPROV_USE_DNSSEC=y UCIPROV_USE_DNS=y UCIPROV_USE_DHCP=y \
 UCIPROV_TGZURI="{uri}/../files{fd}.tgz" \
 UCIPROV_SUURI="{uri}/../su{fd}.img" \
 UCIPROV_URI1="http://eduroamap.{d}/default/uci{fd}" UCIPROV_URI2="http://eduroamap.{d}/{ip}/uci{fd}" UCIPROV_URI3="http://eduroamap.{d}/{mac}/uci{fd}" \
 UCIPROV_RECOVERY=y

OWRT_CONFIG_UNSET += UCIPROV_SU_ONLYFD  UCIPROV_TGZ_ONLYFD


