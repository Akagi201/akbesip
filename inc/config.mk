ARCH=$(shell uname -m)
DEPS_x86:= build-essential asciidoc binutils bzip2 gawk gettext git libncurses5-dev libz-dev patch unzip zlib1g-dev flex quilt scons libxml-parser-perl bison libgmp3-dev antlr openjdk-6-jdk openjdk-6-jre mtd-tools python-dev zip qemu-user-static binfmt-support sudo texinfo
DEPS_x86_64:= build-essential asciidoc binutils bzip2 gawk gettext git libncurses5-dev libz-dev patch unzip zlib1g-dev ia32-libs lib32gcc1 libc6-dev-i386 flex quilt scons libxml-parser-perl bison libgmp3-dev antlr openjdk-6-jdk openjdk-6-jre mtd-tools python-dev zip qemu-user-static binfmt-support sudo texinfo

CP=cp --reflink=auto
RM=rm

BUILD_DIR=$(PWD)/build
DL_DIR=$(PWD)/build/dl
JOBS=6
MAKE_ARGS=
BESIP_GIT_REV=$(shell git rev-parse HEAD)
MAKE_ARGS += BESIP_GIT_REV=$(BESIP_GIT_REV)

BESIP_PACKAGES_DIR=$(BESIP_IMG_DIR)/

# Default packages in image
BESIPPKG_LITE=y
BESIPPKG=y
BESIPPKG_HUGE=m
BESIPPKG_DEV=m

BESIP_RSYNC_MIRROR=rsync://liptel.vsb.cz/besip

OWRT_CONFIG_SET = \
 DEVEL=y \
 LOCALMIRROR=\"http://mirror.opf.slu.cz/openwrt/sources/\" \
 DOWNLOAD_FOLDER=\"$(DL_DIR)\" \
 BROKEN=y \
 IMAGEOPT=y \
 VERSIONOPT=y \
 PREINITOPT=y \
 INITOPT=y \
 VERSION_DIST=\"BeSip\"

OWRT_CONFIG_UNSET += AUTOREBUILD PACKAGE_asterisk14 PACKAGE_kamailio PACKAGE_yuma1-libncx PACKAGE_yuma1-yang-utils 

OWRT_PATCHES = fortify_source.diff lua.patch owrtshell.diff sqlite2.diff

VIRTFS_SECURITY:= security_model=mapped-file
VIRTFS_OPTIONS:= trans=virtio,version=9p2000.L

