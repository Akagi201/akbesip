ifeq ($(ASTPKG),)
 ifeq ($(OWRT_NAME),trunk)
	ASTPKG=asterisk11
 else
	ASTPKG=asterisk18
 endif
endif

ifeq ($(KAMPKG),)
 ifeq ($(BESIP_VERSION),trunk)
	KAMPKG=kamailio4
 else
	KAMPKG=kamailio3
 endif
endif


BESIP_LITE_DEPS += \
  +$(ASTPKG) \
  +sqlite2-cli +sqlite3-cli +nrpe +zabbix-agent +zabbix-agentd +zabbix-sender \
  +lighttpd +lighttpd-mod-cgi +lighttpd-mod-fastcgi +lighttpd-mod-alias +lighttpd-mod-auth +lighttpd-mod-redirect +lighttpd-mod-rewrite +lighttpd-mod-access +lighttpd-mod-proxy \
  +openssl-util +luci +luci-sgi-cgi +wget +kmod-ipv6 +kmod-usb-core +kmod-usb-storage +kmod-usb-serial +kmod-ip6tables +ip6tables \
  +besip-patcher +uciprov
 
BESIP_DEPS += \
  +$(ASTPKG)-gui \
  +$(ASTPKG)-app-chanisavail +$(ASTPKG)-app-chanspy +$(ASTPKG)-app-exec +$(ASTPKG)-app-minivm \
  +$(ASTPKG)-app-originate +$(ASTPKG)-app-read +$(ASTPKG)-app-readexten +$(ASTPKG)-app-sayunixtime \
  +$(ASTPKG)-app-sms +$(ASTPKG)-app-stack +$(ASTPKG)-app-system \
  +$(ASTPKG)-app-talkdetect +$(ASTPKG)-app-verbose +$(ASTPKG)-app-waituntil +$(ASTPKG)-app-while \
  +$(ASTPKG)-chan-iax2 +$(ASTPKG)-chan-mobile +$(ASTPKG)-codec-a-mu \
  +$(ASTPKG)-codec-alaw +$(ASTPKG)-codec-g722 +$(ASTPKG)-codec-g726 +$(ASTPKG)-curl +$(ASTPKG)-format-g726 \
  +$(ASTPKG)-format-g729 +$(ASTPKG)-format-sln +$(ASTPKG)-func-blacklist \
  +$(ASTPKG)-func-channel +$(ASTPKG)-func-cut +$(ASTPKG)-func-db +$(ASTPKG)-func-devstate +$(ASTPKG)-func-extstate \
  +$(ASTPKG)-func-global +$(ASTPKG)-func-shell +$(ASTPKG)-func-uri +$(ASTPKG)-func-vmcount \
  +$(ASTPKG)-pbx-ael +$(ASTPKG)-pbx-spool +$(ASTPKG)-res-ael-share +$(ASTPKG)-res-agi \
  +$(ASTPKG)-res-musiconhold +$(ASTPKG)-cdr +$(ASTPKG)-sounds +$(ASTPKG)-voicemail +$(ASTPKG)-chan-agent \
  +$(KAMPKG) +$(KAMPKG)-mod-acc +$(KAMPKG)-mod-corex +$(KAMPKG)-mod-acc-radius +$(KAMPKG)-mod-alias-db \
  +$(KAMPKG)-mod-auth +$(KAMPKG)-mod-cfg-rpc +$(KAMPKG)-mod-cfgutils +$(KAMPKG)-mod-ctl +$(KAMPKG)-mod-db-flatstore \
  +$(KAMPKG)-mod-db-sqlite +$(KAMPKG)-mod-db-text +$(KAMPKG)-mod-dialog +$(KAMPKG)-mod-dialplan \
  +$(KAMPKG)-mod-diversion +$(KAMPKG)-mod-domain +$(KAMPKG)-mod-enum +$(KAMPKG)-mod-exec +$(KAMPKG)-mod-group \
  +$(KAMPKG)-mod-htable +$(KAMPKG)-mod-ipops +$(KAMPKG)-mod-kex +$(KAMPKG)-mod-lcr +$(KAMPKG)-mod-maxfwd \
  +$(KAMPKG)-mod-mediaproxy +$(KAMPKG)-mod-mi-datagram +$(KAMPKG)-mod-mi-fifo +$(KAMPKG)-mod-mi-rpc \
  +$(KAMPKG)-mod-misc_radius +$(KAMPKG)-mod-msilo +$(KAMPKG)-mod-nat_traversal +$(KAMPKG)-mod-nathelper \
  +$(KAMPKG)-mod-path +$(KAMPKG)-mod-pike +$(KAMPKG)-mod-presence +$(KAMPKG)-mod-pua +$(KAMPKG)-mod-pua-usrloc \
  +$(KAMPKG)-mod-pv +$(KAMPKG)-mod-qos +$(KAMPKG)-mod-ratelimit +$(KAMPKG)-mod-regex +$(KAMPKG)-mod-registrar \
  +$(KAMPKG)-mod-rls +$(KAMPKG)-mod-rr +$(KAMPKG)-mod-rtimer +$(KAMPKG)-mod-rtpproxy +$(KAMPKG)-mod-sanity \
  +$(KAMPKG)-mod-siputils +$(KAMPKG)-mod-sl +$(KAMPKG)-mod-textops +$(KAMPKG)-mod-tls +$(KAMPKG)-mod-tm \
  +$(KAMPKG)-mod-tmx +$(KAMPKG)-mod-userblacklist +$(KAMPKG)-mod-usrloc +$(KAMPKG)-mod-utils \
  +$(KAMPKG)-mod-xcap-client +$(KAMPKG)-mod-xlog +$(KAMPKG)-mod-xmpp +$(KAMPKG)-tools \
  +mediaproxy-common +mediaproxy-relay +mediaproxy-dispatcher \
  +libssh2 +snort +snortsam +libxml2 +sipp \
  +python +python-curl +python-openssl +python-sqlite +python-sqlite3 +pyopenssl \
  +coreutils +coreutils-nohup \
  +unixodbc +sqliteodbc \
  +luci-ssl +luci-mod-admin-core \
  +kmod-fs-vfat +kmod-usb-hid

ifeq ($(OWRT_NAME),trunk)
  BESIP_DEPS += +$(ASTPKG)-app-alarmreceiver +$(ASTPKG)-pbx-lua +$(ASTPKG)-odbc +libnetconf +$(ASTPKG)-chan-dongle
ifeq ($(TARGET_ARCH),x86)
  BESIP_DEPS += +kmod-ata-core +kmod-ata-ahci +tshark
endif
endif

BESIP_HUGE_DEPS += \
  +$(KAMPKG)-mod-auth-db +$(KAMPKG)-mod-auth-radius +$(KAMPKG)-mod-avpops +$(KAMPKG)-mod-benchmark +$(KAMPKG)-mod-cfg-db \
  +$(KAMPKG)-mod-db-mysql +$(KAMPKG)-mod-db-unixodbc +$(KAMPKG)-mod-dispatcher +$(KAMPKG)-mod-domainpolicy \
  +$(KAMPKG)-mod-h350 +$(KAMPKG)-mod-ldap +$(KAMPKG)-mod-pdt +$(KAMPKG)-mod-peering +$(KAMPKG)-mod-permissions \
  +$(KAMPKG)-mod-presence-dialoginfo +$(KAMPKG)-mod-presence-mwi +$(KAMPKG)-mod-presence-xml \
  +$(KAMPKG)-mod-pua-bla +$(KAMPKG)-mod-pua-dialoginfo +$(KAMPKG)-mod-pua-mi +$(KAMPKG)-mod-pua-xmpp \
  +$(KAMPKG)-mod-sipcapture +$(KAMPKG)-mod-siptrace +$(KAMPKG)-mod-sms +$(KAMPKG)-mod-speeddial +$(KAMPKG)-mod-sqlops +$(KAMPKG)-mod-sst \
  +$(KAMPKG)-mod-statistics +$(KAMPKG)-mod-uac +$(KAMPKG)-mod-uac-redirect +$(KAMPKG)-mod-uri-db \
  +fail2ban \
  +php5 +php5-cgi +php5-cli +php5-fastcgi +php5-mod-ctype \
  +php5-mod-curl +php5-mod-dom +php5-mod-exif +php5-mod-fileinfo +php5-mod-ftp \
  +php5-mod-gmp +php5-mod-hash +php5-mod-iconv +php5-mod-json \
  +php5-mod-ldap +php5-mod-mbstring +php5-mod-mcrypt +php5-mod-openssl \
  +php5-mod-pcntl +php5-mod-pdo +php5-mod-pdo-sqlite +php5-mod-session \
  +php5-mod-simplexml +php5-mod-soap +php5-mod-sockets +php5-mod-sqlite \
  +php5-mod-sqlite3 +php5-mod-sysvmsg +php5-mod-sysvsem +php5-mod-sysvshm \
  +php5-mod-tokenizer +php5-mod-xml +php5-mod-xmlreader +php5-mod-xmlwriter \
  +unixodbc-tools \
  +bzip2 +ntpdate \
  +jamvm +classpath \
  +ipv6calc \
  +luci-mod-admin-full \
  +ip \
  +sipml5

BESIP_DEV_DEPS += +strace +gdb


