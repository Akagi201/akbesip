
# Files which are aded during compilation
OWRT_ADDED_FILES += etc/profile:/etc/profile etc/banner:/etc/banner uci-defaults/passwd-prep:/etc/uci-defaults/passwd-prep

# Files which are added during first boot from defaults
OWRT_ADDED_ETCDEFAULT += etc/config etc/besip etc/ssl etc/asterisk etc/lighttpd etc/snort etc/ssh

