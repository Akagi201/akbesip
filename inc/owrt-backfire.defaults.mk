
OWRT_SVN=https://liptel.vsb.cz/svn/openwrt/branches/$(OWRT_NAME)
OWRT_FEEDS_PKG="src-svn packages https://liptel.vsb.cz/svn/openwrt/branches/packages_10.03.2"
OWRT_FEEDS_XWRT="src-svn xwrt http://x-wrt.googlecode.com/svn/branches/backfire_10.03.2/package"
OWRT_FEEDS_LUCI="src-svn luci http://svn.luci.subsignal.org/luci/branches/luci-0.11/contrib/package/"
OWRT_FEEDS_ROUTING=""
OWRT_FEEDS_TELEPHONY=""
OWRT_CONFIG_UNSET += PACKAGE_asterisk10

