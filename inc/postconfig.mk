# Post configuration script for BESIP
# All config and target variables has to be set before including this!

ifneq ($(DBG),)
  $(eval $(call BesipDefaults,dbg))
endif

ifneq ($(VERBOSE),)
  JOBS = 1
  MAKE_ARGS += V=99
endif

# Defaults
REPO_DIR=$(PWD)/build/repo

ifeq ($(OWRT_DIR),)
  OWRT_DIR=$(BUILD_DIR)/owrt-$(TARGET_CPU)-$(OWRT_NAME)$(OWRT_SUFFIX)
endif

OWRT_GIT_REV=$(shell if [ -d "$(OWRT_DIR)" ]; then (cd $(OWRT_DIR) && git rev-parse HEAD); fi)
MAKE_ARGS += OWRT_GIT_REV=$(OWRT_GIT_REV)
ifeq ($(OWRT_NAME),trunk)
  OWRT_CONFIG_SET += BUILD_LOG=y
  TRUNK_GITREV+= $(OWRT_GIT_REV)
 ifneq ($(DBG),)
    TARGET_SUFFIX+=_r$(OWRT_GIT_REV)-dbg
 else
  ifeq ($(BESIP_VERSION),trunk)
    TARGET_SUFFIX+=_r$(OWRT_GIT_REV)
  endif
 endif
endif
MAKE_ARGS += OWRT_VERSION=$(OWRT_NAME) BESIP_VERSION=$(BESIP_VERSION) BESIP_PACKAGES="$(BESIP_PACKAGES)" EMBEDED_MODULES="$(EMBEDED_MODULES)" \
  OWRT_ADDED_ETCDEFAULT="$(OWRT_ADDED_ETCDEFAULT)" \
  OWRT_ADDED_FILES="$(OWRT_ADDED_FILES)"

IMG_DIR=$(OWRT_DIR)/bin/$(TARGET_CPU)
BESIP_IMG_DIR=$(REPO_DIR)/$(BESIP_VERSION)/owrt_$(OWRT_NAME)/$(TARGET_CPU)$(CPU_SUFFIX)
BESIP_PKG_DIR=$(BESIP_IMG_DIR)/packages
OWRT_PKG_DIR=$(IMG_DIR)/packages
BESIP_IMG_PREFIX=besip-$(TARGET_NAME)$(TARGET_SUFFIX)

ifeq ($(SUBTARGET),)
 SUBTARGET=generic
endif

#ImageBuilder
PKG_OS:=$(word 2,$(subst -, ,$(shell $(CC) -dumpmachine)))
PKG_CPU:=$(word 1,$(subst -, ,$(shell $(CC) -dumpmachine)))
ifneq ($(OWRT_IMG_PROFILE),)
 ifeq ($(TARGET_CPU),x86_64)
  IB_NAME:=OpenWrt-ImageBuilder-$(TARGET_CPU)-for-$(PKG_OS)-$(PKG_CPU)
 else
  IB_NAME:=OpenWrt-ImageBuilder-$(TARGET_CPU)_$(SUBTARGET)-for-$(PKG_OS)-$(PKG_CPU)
 endif
 OWRT_CONFIG_SET += IB=y
endif

ifneq ($(OWRT_IMG_SQUASHFS_NAME),)
	OWRT_IMG_SQUASHFS=$(IMG_DIR)/$(OWRT_IMG_SQUASHFS_NAME)
	BESIP_IMG_SQUASHFS=$(BESIP_IMG_DIR)/$(BESIP_IMG_PREFIX).squashfs
endif

ifneq ($(OWRT_IMG_DISK_NAME),)
	OWRT_IMG_DISK=$(IMG_DIR)/$(OWRT_IMG_DISK_NAME)
	BESIP_IMG_VMDK=$(BESIP_IMG_DIR)/$(BESIP_IMG_PREFIX).vmdk
	BESIP_IMG_DISK=$(BESIP_IMG_DIR)/$(BESIP_IMG_PREFIX).img
	BESIP_VMX=$(BESIP_IMG_DIR)/$(BESIP_IMG_PREFIX).vmx
	BESIP_DATA_VMDK=$(BESIP_IMG_DIR)/besip-data.vmdk
	BESIP_KVMSH=$(BESIP_IMG_DIR)/$(BESIP_IMG_PREFIX).kvm.sh
endif

ifneq ($(OWRT_IMG_KERNEL_NAME),)
	OWRT_IMG_KERNEL=$(IMG_DIR)/$(OWRT_IMG_KERNEL_NAME)
	BESIP_IMG_KERNEL=$(BESIP_IMG_DIR)/$(BESIP_IMG_PREFIX).vmlinuz
endif

ifneq ($(OWRT_IMG_FACTORY_NAME),)
	OWRT_IMG_FACTORY=$(IMG_DIR)/$(OWRT_IMG_FACTORY_NAME)
	BESIP_IMG_FACTORY=$(BESIP_IMG_DIR)/$(BESIP_IMG_PREFIX)-factory.bin
endif

ifneq ($(OWRT_IMG_BIN_NAME),)
	OWRT_IMG_BIN=$(IMG_DIR)/$(OWRT_IMG_BIN_NAME)
	BESIP_IMG_BIN=$(BESIP_IMG_DIR)/$(BESIP_IMG_PREFIX)-sysupgrade.bin
endif

ifneq ($(OWRT_IMG_VMDK_NAME),)
	OWRT_IMG_VMDK=$(IMG_DIR)/$(OWRT_IMG_VMDK_NAME)
	BESIP_IMG_VMDK=$(BESIP_IMG_DIR)/$(BESIP_IMG_PREFIX).vmdk
	BESIP_VMX=$(BESIP_IMG_DIR)/$(BESIP_IMG_PREFIX).vmx
	BESIP_DATA_VMDK=$(BESIP_IMG_DIR)/besip-data.vmdk
endif

ifneq ($(OWRT_IMG_ISO_NAME),)
	OWRT_IMG_ISO=$(IMG_DIR)/$(OWRT_IMG_ISO_NAME)
	BESIP_IMG_ISO=$(BESIP_IMG_DIR)/$(BESIP_IMG_PREFIX).iso
endif

ifeq ($(OWRT_IMG_SYSUPGRADE_NAME),)
 OWRT_IMG_SYSUPGRADE=$(IMG_DIR)/$(OWRT_IMG_BIN_NAME)
else
 OWRT_IMG_SYSUPGRADE=$(IMG_DIR)/$(OWRT_IMG_SYSUPGRADE_NAME)
endif
BESIP_IMG_SYSUPGRADE=$(BESIP_IMG_DIR)/$(BESIP_IMG_PREFIX).sysupgrade

OWRT_IMAGES=$(OWRT_IMG_BIN) $(OWRT_IMG_KERNEL) $(OWRT_IMG_DISK) $(OWRT_IMG_SQUASHFS) $(OWRT_IMG_VMDK) $(OWRT_IMG_TRX) $(OWRT_IMG_ISO)
BESIP_IMAGES=$(BESIP_IMG_BIN) $(BESIP_IMG_KERNEL) $(BESIP_IMG_DISK) $(BESIP_IMG_SQUASHFS) $(BESIP_IMG_VMDK) $(BESIP_IMG_FACTORY) $(BESIP_IMG_ISO) $(BESIP_IMG_SYSUPGRADE)

# Load defaults for specific owrt version
$(eval $(call BesipDefaults,owrt-$(OWRT_NAME)))
# Load defaults for besip version
$(eval $(call BesipDefaults,$(BESIP_VERSION)))
# Load defaults for target CPU
$(eval $(call BesipDefaults,$(TARGET_CPU)))

ifeq ($(REPOURL),)
 REPOURL=http://mirror.opf.slu.cz/besip/
endif

OWRT_CONFIG_SET += VERSION_NICK=\"$(BESIP_VERSION)\" VERSION_NUMBER=\"$(BESIP_GIT_REV)\" VERSION_REPO=\"$(REPOURL)/$(BESIP_VERSION)/owrt_$(OWRT_NAME)/$(TARGET_CPU)/packages\"

LOG_DIR=$(PWD)/log/$(TARGET_NAME)$(TARGET_SUFFIX)
KVM_DIR=$(OWRT_DIR)/testkvm/
OWRT_SHELL=$(OWRT_DIR)/scripts/shell.sh

BMAKE=make $(MAKE_ARGS)

$(eval $(call BesipDefaults,packages))

# Construct besip packages
BESIP_PACKAGES += besip=y
BESIP_PACKAGES += $(foreach p,$(BESIP_LITE_DEPS),$(subst +,,$(p))=$(BESIPPKG_LITE))
BESIP_PACKAGES += $(foreach p,$(BESIP_DEPS),$(subst +,,$(p))=$(BESIPPKG))
BESIP_PACKAGES += $(foreach p,$(BESIP_HUGE_DEPS),$(subst +,,$(p))=$(BESIPPKG_HUGE))
BESIP_PACKAGES += $(foreach p,$(BESIP_DEV_DEPS),$(subst +,,$(p))=$(BESIPPKG_DEV))

KVM_DHCPOPTS=tftp=$(KVM_TFTP_DIR),bootfile=$(KVM_BOOT_FILE),dnssearch=$(KVM_DNSSEARCH),hostname=$(KVM_HOSTNAME)
KVM_NICOPTS=-net nic,model=e1000 -net user,hostfwd=tcp::2222-:22,hostfwd=tcp::2223-:23,hostfwd=tcp::2280-:80,hostfwd=tcp::2281-:8088,$(KVM_DHCPOPTS) -net nic,model=e1000
KVM_TFTP_DIR="$(KVM_DIR)/tftp"
KVM_TFTP_TMP="$(KVM_DIR)/tftp/tmp"
QEMU=qemu-system-$(TARGET_QEMU) $(TARGET_QEMU_OPTS)
KVM=qemu-system-$(TARGET_QEMU) $(KVM_NICOPTS) $(TARGET_QEMU_OPTS)

$(eval $(call BesipDefaults,uciprov))

BESIP_PACKAGES:=$(subst ___,\",$(shell ./rmdup.sh BESIP_PACKAGES $(subst ",___,$(BESIP_PACKAGES))))
EMBEDED_MODULES:=$(subst ___,\",$(shell ./rmdup.sh EMBEDED_MODULES $(subst ",___,$(EMBEDED_MODULES))))
OWRT_CONFIG_SET:=$(subst ___,\",$(shell ./rmdup.sh OWRT_CONFIG_SET $(subst ",___,$(OWRT_CONFIG_SET))))
OWRT_CONFIG_UNSET:=$(subst ___,\",$(shell ./rmdup.sh OWRT_CONFIG_UNSET $(subst ",___,$(OWRT_CONFIG_UNSET))))
	
# List of packages embeded into img
BESIP_EPACKAGES=$(foreach pkg,$(filter %=y,$(BESIP_PACKAGES)),$(subst =y,,$(pkg))) $(foreach pkg,$(filter %=n,$(BESIP_PACKAGES)),$(subst =n,,-$(pkg))) $(foreach pkg,$(filter %=m,$(BESIP_PACKAGES)),$(subst =m,,-$(pkg))) 

# Stamps:
STAMP_DIR=$(BUILD_DIR)/stamp/$(TARGET_NAME)$(TARGET_SUFFIX)
OSTAMP_DIR=$(OWRT_DIR)/stamp
SPREREQ=$(STAMP_DIR)/prereq
SGITCLONE=$(OSTAMP_DIR)/gitclone
SFEEDSU=$(OSTAMP_DIR)/feeds-updated
SFEEDSI=$(OSTAMP_DIR)/feeds-installed
SPATCHED=$(OSTAMP_DIR)/owrt-patched
SOWRTPREP=$(OSTAMP_DIR)/owrt-prepared
SKERNPREP=$(STAMP_DIR)/kernel-prepared
SOWRTCFG=$(STAMP_DIR)/owrt-configured
SOWRTIMG=$(STAMP_DIR)/owrt-image-created
SOWRTIMGI=$(STAMP_DIR)/owrt-image-premaked
SOWRTIMGB=$(OSTAMP_DIR)/owrt-imagebuilder
OWRTTCPREP=$(STAMP_DIR)/owrt-toolchain-installed

