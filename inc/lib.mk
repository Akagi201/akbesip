
define owrtenv
$$(cd $(OWRT_DIR); $(OWRT_SHELL) env $1)
endef

define mkdirall
	if [ -d $(OWRT_DIR) ]; then \
	  mkdir -p $(STAMP_DIR) $(OSTAMP_DIR) $(DL_DIR) $(LOG_DIR) $(BESIP_IMG_DIR); \
	fi
endef

define owrt_env
	(cd $(OWRT_DIR); ./scripts/env $1)
endef

define owrt_testconf
	@if [ "$$(wc -l <$(OWRT_DIR)/.config 2>/dev/null)" -lt 50 ]; then $(RM) -f $(SOWRTCFG) $(OWRT_DIR)/.config; $(BMAKE) config-owrt; fi
endef

define BesipDefaults
 ifeq ($(filter $(1),$(BESIP_NODEFAULTS) $(BESIP_DEFAULTS)),)
  -include inc/$(1).defaults$(DEFAULTS.$(1)).mk
  BESIP_DEFAULTS += $(1)
 endif
endef

define AddDirFiles
 $(shell cd files && find $(1) -type f -a -not -name '*~' |while read line; do echo $$line:$$(echo $$line |cut -d '/' -f 2-); done)
endef

define AddFiles
	for i in $(OWRT_ADDED_FILES); do f=$(PWD)/files/$$(echo $$i|cut -d ':' -f 1); t=$(OWRT_DIR)/files/$$(echo $$i|cut -d ':' -f 2); mkdir -p $$(dirname $$t); $(CP) $$f $$t; done
endef

