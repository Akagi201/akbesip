
# Version specific settings
# This file should be includable either via Makefile or via shell!

BESIP_VERSION=trunk
KAMPKG=kamailio3-next
OWRT_FORCE_GIT_REV=master
OWRT_FORCE_PACKAGES_GIT_REV=master
OWRT_FORCE_TELEPHONY_GIT_REV=master
OWRT_FORCE_ROUTING_GIT_REV=master
OWRT_FORCE_LUCI_GIT_REV=master

