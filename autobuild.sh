#!/bin/bash

renice 19 $$ >/dev/null 2>/dev/null
ionice -c 3 -p $$

cd $(dirname $0)
. autobuild.inc.sh
[ -f autobuild.local.sh ] && . autobuild.local.sh

# Read version settings
. inc/version.inc

export oactions="$1"
if echo $1 | grep -qe "," ; then
    if [ -n "$3" ] || [ "$2" = "tall" ] || [ "$2" = "pall" ]; then
      echo "Cannot combine multiple targets and actions yet!"
      exit 2
    fi
    export IFS=",";
    actions="$1"
    shift;
    for i in $actions; do
     $0 $i $*
    done
    exit
fi
export oaction="$1"

if [ "$1" == "tst" ]; then
  TST=y
  arg1=$1
  shift;
fi
if [ "$1" == "build" ]; then
  arg1=$1
  BUILDONLY=y
  shift;
fi
if [ "$1" == "fastbuild" ]; then
  arg1=$1
  FBUILDONLY=y
  shift;
fi
if [ "$1" == "clean" ]; then
  CLEAN=y
  arg1=$1
  shift;
fi
if [ "$1" == "dirclean" ]; then
  DIRCLEAN=y
  arg1=$1
  shift;
fi
if [ "$1" == "update" ]; then
  UPDATE=y
  arg1=$1
  shift;
fi
if [ "$1" == "prepare" ]; then
  PREPARE=y
  arg1=$1
  shift;
fi
if [ "$1" == "config" ]; then
  CONF=y
  arg1=$1
  shift;
fi
if [ "$1" == "debug" ]; then
  MKVERBOSE="VERBOSE=y"
  arg1=$1
  shift;
fi
if [ "$1" == "unlock" ]; then
  UNLOCK=y
  arg1=$1
  shift;
fi

if [ "$1" == "repo" ]; then
  mkrepo
fi


if [ "$1" == "shell" ]; then
  TESTSHELL=y
  shift;
fi

if [ "$1" == "repoclean" ]; then
  cleanrepo
fi

if [ "$1" == "all" ]; then
  shift;
  for target in $(gettargets); do
    $0 $arg1 $target $* 2>&1 |tee log/$target.log
  done
  exit
fi 

if [ "$1" == "pall" ]; then
  shift;
  for target in $(gettargets); do
    $0 $arg1 $target $* &
  done
  exit
fi 

if [ "$1" == "tall" ]; then
  shift;
  if ! $TMUX has-session; then
    echo "You have to run tall target inside tmux!"
    exit 2
  fi
  $TMUX rename-session "autobuild_$(basename $PWD)"
  $TMUX rename-window "$oaction"
  for target in $(gettargets); do
    hw=$(targethw $target)
    arch=$(targetarch $target)
    owrt=$(targetowrt $target)
    waitfortmuxjobs "$JOBS" "$arch-$owrt"
    $TMUX new-window -kd -n "$target" "$0 $arg1 $target $*" && echo "See window $target in tmux ($0 $arg1 $target $*)." & sleep 1
    $TMUX rename-window "$oaction"
  done
  exit
fi 

if [ "$1" == "all-dbg" ]; then
  shift;
  for target in $(gettargetsdbg); do
    $0 $arg1 $target $*
  done
  exit
fi 

if [ "$1" == "tall-dbg" ]; then
  shift;
  if ! $TMUX has-session; then
    echo "You have to run tall target inside tmux!"
    exit 2
  fi
  $TMUX rename-session "autobuild_$(basename $PWD)"
  $TMUX rename-window "$oaction"
  for target in $(gettargetsdbg); do
    hw=$(targethw $target)
    arch=$(targetarch $target)
    owrt=$(targetowrt $target)
    waitfortmuxjobs "$JOBS" "$arch-$owrt"
    $TMUX new-window -kd -n "$target" "$0 $arg1 $target $*" && echo "See window $target in tmux ($0 $arg1 $target $*)." & sleep 1
    $TMUX rename-window "$oaction"
  done
  exit
fi 

# Multiple targets on commandline - use tmux
if [ -n "$2" ]; then
  if ! $TMUX has-session; then
    echo "You have to run tall target inside tmux!"
    exit 2
  fi
  $TMUX rename-session "autobuild_$(basename $PWD)"
  $TMUX rename-window "$oaction"
  for target in $*; do
    hw=$(targethw $target)
    arch=$(targetarch $target)
    owrt=$(targetowrt $target)
    waitfortmuxjobs "$JOBS" "$arch-$owrt"
    $TMUX new-window -kd -n "$target" "$0 $arg1 $target" && echo "See window $target in tmux ($0 $arg1 $target)." & sleep 1
    $TMUX rename-window "$oaction"
  done
  exit
fi

TARGET="$1"
if echo $TARGET| grep -qE "\-dbg"; then
  TARGET=$(echo $TARGET| cut -d - -f 1-3)
  export MDBG="DBG=y"
  export TSUFFIX="-dbg"
fi

if ! [ -f "$PWD/targets/$TARGET.mk" ]; then
  echo "Enter valid target name! ($0 [actions] target [target] ...)"
  echo 
  echo "actions can be (could be several actions separated by comma):"
  echo "build - build (default action)"
  echo "fastbuild - build only BESIP packages and regenerate image"
  echo "tst - only show what would be done."
  echo "clean - clean target."
  echo "dirclean - directory clean target (delete all compiled files except downloaded archives)."
  echo "update - update target from svn."
  echo "prepare - prepare target tools and toolchain."
  echo "config - only configure target."
  echo "debug - make with verbose messages (single thread)."
  echo "unlock - remove lock file"
  echo "shell - try to emulate shell of compiled rootfs"
  echo "repo - Create repository from local builds"
  echo "repoclean - Clean repo"
  echo
  echo "Targets: (you may use 'all' to build all, 'pall' to paralel build all, 'tall' to paralel build with tmux)"
  gettargets
  echo
  echo "Targets with debug info (only for BESIP development)"
  gettargetsdbg
  echo
  exit 2
fi

TARGETCFG="$PWD/targets/$TARGET.mk"
LOCKFILE="build/auto-${TARGET}${TSUFFIX}.lock"
export BCPU=$(echo $TARGET | cut -d '-' -f 2)
export BHW=$(echo $TARGET | cut -d '-' -f 1)
export OWRT=$(echo $TARGET | cut -d '-' -f 3)

if [ -n "$UNLOCK" ]; then
  rm -f "$LOCKFILE"
  exit
fi

OWRT_DIR=$PWD/build/owrt-${BCPU}-${OWRT}${TSUFFIX}
MSEPARATE="OWRT_DIR=${OWRT_DIR}"

$TMUX set-option remain-on-exit off >/dev/null 2>/dev/null
$TMUX rename-window "$TARGET" >/dev/null 2>/dev/null

if [ -n "$UPDATE" ]; then
  updatetarget
  exit $?
fi

if [ -n "$PREPARE" ]; then
  preparetarget
  exit $?
fi

if [ -n "$CLEAN" ]; then
  cleantarget
  exit $?
fi
if [ -n "$DIRCLEAN" ]; then
  dircleantarget
  cleanrepo
  exit $?
fi

if [ -n "$CONF" ]; then
  configtarget
  exit $?
fi

if [ -n "$TESTSHELL" ]; then
  shelltarget
  exit $?
fi

if [ -z "$TST" ]; then
  if [ -z "$BUILDONLY" ] && [ -z "$FBUILDONLY" ]; then 
    cleantarget
    updatetarget
    configtarget
  fi
  _yes | buildtarget
else
  buildtarget
fi

