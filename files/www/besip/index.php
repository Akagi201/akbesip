<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="cs-CZ">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="files/style.css" type="text/css" media="all" />
	<title>BESIP - Guidepost</title>
</head>
<body>
<div id="obal">
<div class="loga">
	<a href="http://liptel.vsb.cz/"><img src="files/logo-liptel.png" title="liptel logo" alt="liptel logo"/></a>
	<a href="http://www.cesnet.cz/"><img src="files/cesnet-logo-200.gif" title="cesnet logo" alt="cesnet logo"/></a>
</div>
<div class="linkform">
	<h1><a href="http://homeproj.cesnet.cz/projects/besip">BESIP</a></h1>
	<h2>Guidepost to applications and administration pages:</h2>
	<div class="link">
		<a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/static/config/index.html">Asterisk - administration</a>
	</div>
	<div class="link">
		<a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/cgi-bin/luci/">OpenWrt LuCI - System administration</a>
	</div>
	<div class="link">
		<a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/besip/qual/">Quality monitoring</a>
	</div>
</div>
</div>

</body>
</html>
