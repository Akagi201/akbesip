
. /lib/uciprov/functions.sh

#
# logging function
#

blog() {
        logger -s -t "[Besip]" $*
}

# Besip environment get

besip_env() {
        local section="directories"
        config_get BESIP_CONFDBDIR "${section}" confdbdir /var/besip
        config_get BESIP_VARBDIR "${section}" vardbdir /var/besip
        config_get BESIP_STATDBDIR "${section}" confdbdir /var/besip
        config_get BESIP_STAMPDIR "${section}" confdbdir /var/lib/besip
}

#
# blocking function checking validity of wan addr
#

wait_for_net_addr() {
        while true
        do
                IP=$(ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
                TEST=`echo "${IP}." | /bin/busybox grep -E "([0-9]{1,3}\.){4}"`

                if [ "$TEST" ]
                then
                        echo "$IP" | awk -F. '{
                                if ( (($1>=0) && ($1<=255)) &&
                                        (($2>=0) && ($2<=255)) &&
                                        (($3>=0) && ($3<=255)) &&
                                        (($4>=0) && ($4<=255)) ) {
                                        print($0 " is a valid IP address.");
                                } else {
                                        print($0 ": IP address out of range!");
                                }
                        }'
                        break
                else
                        echo "${IP} is not a valid IP address!"
                        sleep 5
                fi
        done
}

printMacAddress(){
    macAddr=$(cat /sys/class/net/$1/address)
    echo -ne $macAddr
}

printIP(){
    ip=$(ifconfig $1| grep -o "inet addr:[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*"| grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*")
    echo -ne $ip
}

printNetInterfaces(){
    ifaces=$(ls /sys/class/net/)
    echo -ne $ifaces
}

printRootfsUsage(){
    echo -ne $(df -h |grep rootfs|grep -o "[0-9]*%")
}

printFreeMemory(){
    echo -ne $(free | awk '/^Mem:/{print $3}')
}

printAvailableMemory(){
    echo -ne $(free | awk '/^Mem:/{print $2}')
}

printProcessorLoad(){
    echo -ne $(uptime |awk -F'average:' '{ print $2}')
}

sysinfo(){
    echo -e "  System information as of "$(date)"\n"
    ifaces=$(printNetInterfaces)

    for i in $ifaces
    do
            echo -e "  Interface "$i":"
            echo -e "\t\t\t"$(printIP $i)
            echo -e "\t\t\t"$(printMacAddress $i)
    done

    echo -e "  Rootfs usage:\t\t"$(printRootfsUsage)
    echo -e "  Memory usage:\t\t"$(printFreeMemory) "KBytes used of" $(printAvailableMemory) "KBytes available"
    echo -e "  Processor load: \t"$(printProcessorLoad)"\n"
    if ! check_overlay; 
    then
	echo -e "  Overlay not mounted!"
    fi

}

# function besip_help
#
# returns basic parameters for a besip command

besip_help() {
cat <<EOF

  Besip commandline utility.

  Usage:
    besip help
	- This command prints help for besip command

    besip {factory-defaults,fd}
	- This command on squashfs filesystems restores system
	  to initial state.

    besip patch
	- This command updates besip to latest patchlevel for this release.

    besip {crash-reporting,cr} {start,stop,collect}
	- This command controls crash-reporting system. If collect
	  parameter is called, it creates an archive of overlay,
	  collected logs and overlay to be used for debugging in
	  /root/ directory.

    besip security {start,stop}
	- This command controls IDS/IPS Besip system

    besip {system-upgrade,su} {path to BESIP firmware image}
	- This command upgrades system with provided firmware image.

    besip import-gateways {stdout}
	- This command imports VoIP gateways into specified engine
	  in /etc/config/besip. Optionally you can run it with stdout
	  parameter which only prints requested data to stdout.

    besip setup-dialplan
	- This command prepares SIP prefixes to /etc/asterisk/sip.conf.

    besip sysinfo
	- This command prints on stdout system information as free
	  memory, cpu usage or disk usage.

EOF
}

# function crash_reporting
#
# params:
# start - starts crash reporting
# stop - stops crash reporting
# collect - creates a tgz archive

besip_cr(){
        case "$1" in
                start)          touch /.init_enable_core
                                mkdir -p /tmp/coreDump/
                                sysctl -w "kernel.core_pattern=/tmp/coreDump/%e.%p.%s.%t.core" >> /dev/null 2>&1
                                ulimit -c unlimited
                                echo "Crash reporting has started"
                                ;;
                stop)           echo "Crash reporting has stopped"
                                echo "Not yet implemented."
                                ;;
                collect)        echo "Collecting /overlay, /var/log and /tmp/coreDump into /root/besip-"$(uname -m)"-"$(date +%H:%M:%S-%d%m%Y)"-archive.tgz"
                                tar -czf /root/besip-"$(uname -m)"-r"$BESIP_REVISION"-"$(date +%H:%M:%S-%d%m%Y)"-archive.tgz /overlay/ /var/log/ /tmp/coreDump/  >> /dev/null 2>$1
                                ;;
        esac
}



print_gateways(){
	config_load "besip"

	config_get engine "gateways" import_for_engine
	config_get uri "gateways" import_uri

	if [ "$engine" == "" ]; then
		# default cfg engine
		engine="ast11"
	fi

	if [ "$uri" == "" ]; then
		# default uri to gateway database export
		uri=https://iptelix.cesnet.cz/
	fi

	# Handle export for each voip engine
	if [ "$engine" == "ast11" ]; then
		echo "--------------------"
		echo "Asterisk SIP trunks - /etc/asterisk/besip-sip.conf :"
		echo -e "--------------------\n"
		wget -q -O - $uri"/export/asterisk/11/sip.conf"
		echo "--------------------"
		echo "Inclusion of besip-sip-gateways.conf into sip.conf - a part of /etc/asterisk/sip.conf :"
		echo -e "--------------------\n"
		echo "#include \"besip-sip.conf\""
		echo "------------------------"
		echo "Asterisk SIP extensions - /etc/asterisk/besip-extensions-gateways.conf:"
		echo -e "------------------------\n"
		wget -q -O - $uri"/export/asterisk/11/extensions.conf"
		echo "--------------------"
		echo "Inclusion of besip-extensions.conf into extensions.conf - a part of /etc/asterisk/extensions.conf :"
		echo -e "--------------------\n"
		echo "#include \"besip-extensions.conf\""
	elif [ "$engine" == "ast12"]; then
		"Not implemented yet."
	else
		"Not implemented yet. This should be default for any asterisk engine."
	fi
}

import_gateways(){
	config_load "besip"

	config_get engine "gateways" import_for_engine
	config_get uri "gateways" import_uri

	if [ "$engine" == "" ]; then
		# default cfg engine
		engine="ast11"
	fi

	if [ "$uri" == "" ]; then
		# if uri is not set, then set default uri to gateway database export
		uri=https://iptelix.cesnet.cz/
	fi

        # Handle export for each voip engine
	if [ "$engine" == "ast11" ]; then
		echo "Importing gateways.."
		echo -ne "Creating /etc/asterisk/besip-sip-gateways.conf"
		wget -q --output-document=/etc/asterisk/besip-sip-gateways.conf $uri"/export/asterisk/11/sip.conf"
		echo -e "\t\tOK"
		echo -ne "Creating /etc/asterisk/besip-extensions-gateways.conf"
		wget -q --output-document=/etc/asterisk/besip-extensions-gateways.conf - $uri"/export/asterisk/11/extensions.conf"

		echo -e "\tOK"

		if [ -f /etc/asterisk/sip.conf ]; then
			if !(cat /etc/asterisk/sip.conf |grep -q besip-sip-gateways.conf); then
				echo "File besip-sip-gateways.conf was not included in sip.conf. It is now."
				echo "#include \"besip-sip-gateways.conf\"" >> /etc/asterisk/sip.conf
			fi
		else
			echo "File besip-sip-gateways.conf was not included in sip.conf. It is now."
			echo "#include \"besip-sip-gateways.conf\"" >> /etc/asterisk/sip.conf
		fi

		if [ -f /etc/asterisk/extensions.conf ]; then
			if !(cat /etc/asterisk/extensions.conf |grep -q besip-extensions.conf); then
				echo "File besip-extensions.conf was not included in extensions.conf. It is now."
				echo "#include \"besip-extensions.conf\"" >> /etc/asterisk/extensions.conf
			fi
		else
			echo "File besip-extensions.conf was not included in extensions.conf. It is now."
			echo "#include \"besip-extensions.conf\"" > /etc/asterisk/extensions.conf
		fi

		/etc/init.d/asterisk restart
	elif [ "$engine" == "ast12"]; then
		"Not implemented yet."
	else
		"Not implemented yet. This should be default for any asterisk engine."
	fi
}

besip_import_gateways(){
	if [ "$1" == "stdout" ]; then
		print_gateways
	else
		import_gateways
	fi
}

besip_setup_dialplan(){
	config_load "besip"
	config_get engine "gateways" import_for_engine

	if [ "$engine" == "" ]; then
		engine="ast11"
	fi

	if [ "$engine" == "ast11" ]; then
		echo "Preparing configuration.."
		cp /etc/besip/etcdefault/asterisk/besip-extensions.conf /etc/asterisk/besip-extensions.conf

		if [ -f /etc/asterisk/extensions.conf ]; then
			if !(cat /etc/asterisk/extensions.conf |grep -q besip-extensions.conf); then
				echo "File besip-extensions.conf was not included in extensions.conf. It is now."
				echo "#include \"besip-extensions.conf\"" >> /etc/asterisk/extensions.conf
			fi
		else
			echo "File besip-extensions.conf was not included in extensions.conf. It is now."
			echo "#include \"besip-extensions.conf\"" > /etc/asterisk/extensions.conf
                fi

		echo "Preparing prefixes.."

		config_get countrycode "gateways" country_code
		config_get prefix "gateways" prefix

		sed -i "s/E164PREFIX/$countrycode$prefix/g" /etc/asterisk/besip-extensions.conf
		sed -i "s/LOCALPREFIX/$prefix/g" /etc/asterisk/besip-extensions.conf
		sed -i "s/COUNTRYCODE/$countrycode/g" /etc/asterisk/besip-extensions.conf

		echo "Restarting asterisk.."

		/etc/init.d/asterisk restart
	fi
}

besip_factorydefaults() {
	if kernel_getflag overlay;
	then
		dd if=/dev/zero bs=1M count=1 >$(kernel_getvar overlay) && reboot -f
	else
		yes|jffs2reset && reboot -f
	fi
}

besip_sysupgrade() {
	if [ -z "$1" ]; then
		config_load besip
		config_get base_uri "global" base_uri
		config_get sysupgrade_img "global" sysupgrade_img
		if [ -n "$base_uri" ] && [ -n "$sysupgrade_img" ]; then
			cd /tmp && wget "${base_uri}/${sysupgrade_img}" && syupgrade ${sysupgrade_img}
		else
			echo "ERROR: Please, provide path to image file."
		fi
	else
		if [ -f "$1" ]; then
			sysupgrade -v "$1"
		else
			echo "ERROR: Upgrade BESIP image not found"
		fi
	fi
}

# Prepare BESIP after clean factory defaults

besip_prepare() {
	blog "Preparing Besip ..."
	ssl_rehash
	import_gateways
}

besip_init() {
        local enabled
        local prepared
	local crashreport
	config_load besip
        config_get_bool enabled "global" enable 1
	config_get_bool prepared "global" prepared 0
	config_get_bool crashreport "global" crashreport 0

        [ "${enabled}" -eq 0 ] && { echo "BESIP disabled"; return 1; }
        [ "${prepared}" -eq 0 ] && besip_prepare && uci set besip.besip.prepared=1 && uci commit
	if [ "${crashreport}" -eq 1 ];
	then
		besip_cr start
		echo "Besip: starting crash reporting"
	fi
        blog "Great init of Besip"
}



