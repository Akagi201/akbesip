#!/usr/bin/env python
"""
PyPaNeC - Python Passive Netconf Client

    @author: Jiri Slachta
    @version: 0.0.2
"""

import threading
import sys
import getopt
from pyuci.pyucibin import uci
from monitors import uciMon
from monitors import ncMon
from daemon import Daemon
from ncclient.transport import SSHSession
from ncclient import operations

required_args = ['xmlns', 'user', 'password', 'key', 'allowedcfgs']

def usage():
    print "Help for PyPaNeC \n"
    print "How to start PyPaNeC:"
    print "1) execute it without arguments."
    print "    - initial configuration from UCI stored in /etc/config/pypanec"
    print "2) execute with arguments:"
    print "    - host = your hostname"
    print "    - password = password to ssh"
    print "    - allowedcfgs = configuration files to be monitored"
    print "    - xmlns = your xmlns"
    print "    - key = private key\n"
    print "How to stop/reload PyPaNeC:"
    print "    - python pypanec.py stop/reload\n"

def handle_console_args(arr):
    try:
        opts, args = getopt.getopt(arr, 'h:u:p:k:a:x', ['host=', 'user=', 'password=', 'key=', 'allowedcfgs=', 'xmlns='])
    except getopt.GetoptError:
        sys.exit(2)

    for i in args:
        if '=' in i:
            key,value = i.split('=')
            console_values[key]=value

    try:
        for i in required_args:
            if i not in console_values:
                raise Exception('Missing input arguments!')
    except Exception as inst:
        print inst.args[0]
        usage()
        sys.exit(2)

def handle_uci():
    for i in required_args:
        console_values[i]=[x.strip() for x in UciBin.get('pypanec', '@pypanec[0]', i)[0].split(',')]

    for key, value in console_values.items():
        if len(value) == 1:
            console_values[key] = value[0]
        else:
            console_values[key] = value

class Pypanec(Daemon):
    def __init__(self, pidfile, args, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.pidfile = pidfile
        self.console_args = args

    def run(self):
        mutex = threading.Lock()
        ssh_session = SSHSession(self)
        ssh_session.connect('127.0.0.1', port=830, username=self.console_args['user'], password=self.console_args['password'], key_filename=self.console_args['key'])

        thread1 = uciMon(mutex, ssh_session, self.console_args['xmlns'], self.console_args['allowedcfgs'], ['IN_MODIFY','IN_CREATE'])
        thread1.start()

        thread2 = ncMon(mutex, ssh_session)
        thread2.start()

if __name__ == "__main__":
    console_values = dict()

    # zero arg     - start with uci cfg
    # one arg      - stop, restart, help
    # more args    - start with console args

    if len(sys.argv) < 2:
        handle_uci()
        daemon = Pypanec('/tmp/pypanec.pid', console_values)
        print "Starting PyPaNeC with config from UCI"
        daemon.start()
        #daemon.run()
    elif len(sys.argv) == 2:
        if 'stop' == sys.argv[1]:
            daemon = Pypanec('/tmp/pypanec.pid', console_values)
            print "Stopping PyPaNeC"
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon = Pypanec('/tmp/pypanec.pid', console_values)
            print "Reloading PyPaNeC"
            daemon.restart()
        elif ('help' in sys.argv[1]) or ('-h' in sys.argv[1]):
            usage()
    elif len(sys.argv) > 2:
        handle_console_args(sys.argv)
        daemon = Pypanec('/tmp/pypanec.pid', console_values)
        print "Starting PyPaNeC with config from console args"
        daemon.start()
        #daemon.run()
