#!/usr/bin/env python
# -*- coding: utf-8 -*-

import threading
import os
import struct
import sys
import datetime
import errno
from inotifyx import *
from pyucibin import *
from xml.dom.minidom import *
from ncclient.transport import SSHSession
from ncclient import operations
from ncclient.operations.rpc import RPCError

class uciMon(threading.Thread):
    verbose = False
    path = '/etc/config/'

    def __init__(self, session, mutex, xmlns_prefix='http://homeproj.cesnet.cz/projects/besip/uci/', supported_cfg=['network'], fs_flags=['IN_MODIFY','IN_CREATE']):
        if self.verbose: print 'initializing uciMon'
        threading.Thread.__init__(self)
        self.session = session
        self.mutex = mutex
        self.xmlns_prefix = xmlns_prefix
        self.mask = fs_flags
        self.supported_cfg = supported_cfg

    def check_mask(self,event_type, mask):
        events = event_type.split("|")

        for event in events:
            if event in mask:
                return True
        return False

    def create_config_xml(self, config):
        """Returns generated xml file filled by data from UCI configuration file

        Keyword arguments:
        config -- UCI configuration file

        """
        uci_list = Uci.show(config, None, None)

        dom_impl = getDOMImplementation()
        doc = dom_impl.createDocument(None, config, None)
        top_element = doc.documentElement
        top_element.setAttribute('xmlns', self.xmlns_prefix)
        inner_child_elements = list()

        for line in uci_list:
            params, data = line.split('=')
            parameters = params.split('.')

            if params.count('.') == 1:
                child = doc.createElement(data)
                child.setAttribute('name', parameters[1])
                top_element.appendChild(child)
                inner_child_elements.append(child)

        i=-1
        for line in uci_list:
            params, data = line.split('=')
            parameters = params.split('.')

            if params.count('.') == 1:
                i+=1
            elif params.count('.') == 2:
                child = doc.createElement(parameters[2])
                inner_child_elements[i].appendChild(child)
                text = doc.createTextNode(data)
                child.appendChild(text)

        return doc.toprettyxml()

    def is_in_list(self, item, array):
        """Returns true, if item exists in substring element of an array. If does not exist, returns false.

        Keyword arguments:
        item - searched item
        array - array we are looking for an item

        """
        return any(s for s in array if item in s)

    def push_config(self, session, xml):
        try:
            if session.isConnected:
                try:
                    reply = operations.Validate(session).request(source='file://new_config')
                    if not reply.ok:
                        raise reply.error
                    with operations.LockContext(session, target='running'):
                        cc1 = operations.CopyConfig(session).request(source='running', target='file://backup.conf')
                        if not cc1.ok:
                            raise cc1.error

                        cc2 = operations.CopyConfig(session).request(source='file://new_config', target='running')
                        if not cc2.ok:
                            raise cc2.error

                        getreply = operations.GetConfig(session).request(source='running',filter=('xpath', '//the/xpath/filter'))
                        if getreply.ok:
                            print(getreply.data)
                        else:
                            raise getreply.error
                except RPCError as e:
                    print(e)
            else:
                raise Exception('Session error - not connected to NETCONFd!')
        except Exception as inst:
                print inst.args[0]
                sys.exit(2)

    def write_config(self, xml, path):
        text_file = open(path, "w")
        text_file.write(xml)
        text_file.close()

    def run(self):
        print "starting uciMon thread"
        fd = init()

        try:
            wd = add_watch(fd, self.path)
            try:
                while True:
                    events = get_events(fd)

                    for event in events:
                        # extract event name and fs event type from event
                        filename = event.name
                        event_type = event.get_mask_description()
                        # check if the event is the event for specific file
                        if (filename is not None) and self.is_in_list(filename, self.supported_cfg):
                            if self.verbose:
                                print "file name: "+filename
                                print "event: "+event_type

                            # if event type matches the mask, then config xml is created

                            if self.check_mask(event_type, self.mask):
                                xml = self.create_config_xml(filename)
                                self.push_config(xml)
            except KeyboardInterrupt:
                pass
        finally:
            os.close(fd)

class ncMon(threading.Thread):

    def __init__(self, mutex, ssh_session):
        threading.Thread.__init__(self)
        self.mutex = mutex
        self.ssh_session = ssh_session

    def run(self):
        print "ncmon - code here"