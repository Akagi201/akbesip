#!/usr/bin/env python

from distutils.core import setup
setup(name='pypanec',
      version='0.0.0',
      package_dir={'pypanec': 'src'},
      packages=['pypanec'],
      )
