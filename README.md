BESIP project - Bright Embedded SIP solution for embedded telephony
===================================================================

This work has been supported by the CESNET and the Ministry of Education of the Czech Republic within the project LM2010005.

The BESIP is based on OpenWrt.

This is the README for build system for BESIP.

Introduction
------------

Our build system consists of a set of scripts and Makefiles for automated and simplified building OpenWrt with a specific set of scripts and packages for specific target devices.

How to use it
-------------

The autobuild script makes everything you need to build an image of BESIP. There are several actions that could be passed to this script which determines what you could do with specific target.

Simply running ./autobuild in specific branch you get the list of commands you can do and a list of targets you can build.

For example you can build x86 besip image by invoking a command:

./autobuild build virtual-x86-trunk

You can also call several commands separated by a comma in one build set to one target.

If you need to create your own target, you can take as an example existing targets. Target file is a simple .mk file stored in target directory in specific branch which consists of a set of variables that tells our build system what arch, target and packages has to be built.
