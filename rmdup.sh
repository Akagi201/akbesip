#!/bin/bash

null="__Empty__"
i=1;
name=$1;shift

for v in "$@"; do
  export var=$(echo $v |cut -d '=' -f 1)
  export value=$(echo $v |cut -d '=' -f 2)
  [ "$var" = "$value" ] && value="$null"
  shvar=$(echo $var |tr '-' '_')
  shvarindex=$(eval echo \$_$shvar)
  shvarvalue=${values[$shvarindex]}
  if [ -n "$shvarvalue" ]; then
     if [ "$shvarvalue" == "$null" ]; then
       echo "$name: Already set: $var" >&2
     else
       echo "$name: Already set: $var=$shvarvalue, changing to $var=$value" >&2
     fi
     values[$shvarindex]=$value
  else
    export _$shvar=$i
    vars[$i]=$var
    values[$i]=$value
    i=$(expr $i + 1)
  fi
done

i=1
while [ -n "${vars[$i]}" ]; do
  if [ "$value" != "$null" ]; then
    echo "${vars[$i]}=${values[$i]}"
  else
    echo "${vars[$i]}"
  fi
  i=$(expr $i + 1)
done 
