#!/bin/sh

BESIPDIR="$1"; shift
STAMPDIR="$1"; shift

mkstamp () {
  echo $i | tr '/ .' '___'
}

case $1 in 
"patch")
  shift;
  for i in $*; do
    stamp=$(mkstamp "$i") 
    if ! [ -f "$STAMPDIR/$stamp" ]; then
      echo "BESIP PATCH $BESIPDIR/patches/$i ($STAMPDIR/$stamp does not exists)" >&2
      patch -p0 <$BESIPDIR/patches/$i
      touch "$STAMPDIR/$stamp"
      echo "-----"
    else
      echo "$i already patched, skipping patch"
    fi
  done
  ;;
"unpatch")
  shift;
  for i in $*; do 
    stamp=$(mkstamp "$i") 
    if [ -f "$STAMPDIR/$stamp" ]; then
      echo "BESIP UNPATCH $BESIPDIR/patches/$i ($STAMPDIR/$stamp exists)" >&2
      patch -R -p0 <$BESIPDIR/patches/$i
      rm -f "$STAMPDIR/$stamp"
    else
      echo "$i not patched, skipping unpatch"
    fi
  done
  ;;
"patch-backfire"|"patch-trunk"|"patch-attitude_adjustment")
  suite=$(echo $1 | cut -d '-' -f 2)
  for i in $BESIPDIR/patches/$suite/*patch $BESIPDIR/patches/$suite/*diff ; do
    stamp=$(mkstamp "$i") 
    if ! [ -f "$STAMPDIR/$stamp" ]; then
      echo "BESIP PATCH($suite) $i ($STAMPDIR/$stamp does not exists)" >&2
      patch -p0 <$i
      touch "$STAMPDIR/$stamp"
      echo "-----"
    else
      echo "$i already patched, skipping patch"
    fi
  done
  ;;
"unpatch-backfire"|"unpatch-trunk"|"unpatch-attitude_adjustment")
  suite=$(echo $1 | cut -d '-' -f 2)
  for i in $BESIPDIR/patches/$suite/*patch $BESIPDIR/patches/$suite/*diff; do
    stamp=$(mkstamp "$i") 
    if [ -f "$STAMPDIR/$stamp" ]; then
      echo "BESIP UNPATCH $i ($STAMPDIR/$stamp exists)" >&2
      patch -R -p0 <$i
      rm -f "$STAMPDIR/$stamp"
    else
      echo "$i not patched, skipping unpatch"
    fi
  done
  ;;
esac

