TARGET_CPU=lantiq
OWRT_NAME=trunk
TARGET_NAME=wav281_$(BESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=mips
TARGET_QEMU_OPTS=-m64

BESIPPKG=n
BESIPPKG_LITE=n
BESIPPKG_HUGE=n
BESIPPKG_DEV=n

OWRT_IMG_BIN_NAME=openwrt-lantiq-xway-ARV4520PW-squashfs.image
OWRT_IMG_FACTORY_NAME=openwrt-lantiq-xway-ARV4520PW-squashfs.image

OWRT_IMG_KERNEL_NAME=openwrt-lantiq-xway-ARV4520PW-uImage

BESIP_NODEFAULTS=packages uciprov

BESIP_PACKAGES= asterisk11=y luci=y

OWRT_CONFIG_SET += TARGET_lantiq_xway_ARV4520PW=y CONFIG_TARGET_lantiq_xway=y TARGET_lantiq=y

