TARGET_CPU=x86
OWRT_NAME=attitude_adjustment
TARGET_NAME=virtual_$(BESIP_VERSION)-owrt_$(OWRT_NAME)

# Load defaults for x86
$(eval $(call BesipDefaults,x86))
# Load defaults for virtual appliance
$(eval $(call BesipDefaults,virtual))

TARGET_QEMU=i386
TARGET_QEMU_OPTS=-m 512

# Select right versions of core packages
KAMPKG=kamailio3-next
ASTPKG=asterisk10

OWRT_IMG_DISK_NAME=openwrt-$(TARGET_CPU)-generic-combined-squashfs.img
OWRT_IMG_KERNEL_NAME=openwrt-$(TARGET_CPU)-generic-vmlinuz

BESIP_PACKAGES= \
      kmod-usb-net=y \
      block-mount=y \
      kmod-ata-core=y kmod-libsas=y kmod-loop=y kmod-ide-core=y kmod-ata-nvidia-sata=y kmod-ata-piix=y kmod-ata-via-sata=y kmod-ata-ahci=y \
      valgrind=m bash-completion=y bluez-utils=y swap-utils=y fdisk=y e2fsprogs=y resize2fs=y tune2fs=y kmod-fs-btrfs=y btrfs-progs=y kmod-fs-ext4=y kmod-crypto-core=y \
      gnugk=y

OWRT_CONFIG_SET += \
      TOOLCHAINOPTS=y \
      MC_EDITOR=y MC_DIFF_VIEWER=y \
      TARGET_ROOTFS_PARTNAME=\"/dev/sda2\" \
      EXTROOT_SETTLETIME=2

ifeq ($(DBG),)
  OWRT_CONFIG_SET +=  TARGET_ROOTFS_PARTSIZE=192
endif

OWRT_CONFIG_UNSET += MC_DISABLE_VFS 

#OWRT_ADDED_FILES += fstab.virtual:/etc/config/fstab



