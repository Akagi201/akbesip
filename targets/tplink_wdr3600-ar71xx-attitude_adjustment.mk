TARGET_CPU=ar71xx
OWRT_NAME=attitude_adjustment
TARGET_NAME=tplink3600nd_$(BESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=mips
TARGET_QEMU_OPTS=-m64

BESIPPKG=m
BESIPPKG_LITE=y
BESIPPKG_HUGE=m
BESIPPKG_DEV=m

OWRT_IMG_BIN_NAME=openwrt-ar71xx-generic-tl-wdr3600-v1-squashfs-factory.bin
OWRT_IMG_KERNEL_NAME=openwrt-$(TARGET_CPU)-generic-vmlinux.elf

BESIP_PACKAGES= kmod-usb-core=y

OWRT_CONFIG_SET += TARGET_ar71xx=y TARGET_ar71xx_generic_TLWDR4300=y 
