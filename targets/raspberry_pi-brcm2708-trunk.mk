TARGET_CPU=brcm2708
OWRT_NAME=trunk
TARGET_NAME=raspi_$(BESIP_VERSION)-owrt_$(OWRT_NAME)

# Kamailio package used for build (kamailio3 or kamailio4)
KAMPKG=kamailio4

TARGET_QEMU=arm
TARGET_QEMU_OPTS=-cpu arm1176 -m 256 -M versatilepb -no-reboot -serial stdio -append "root=/dev/sda2 panic=1 rootfstype=ext4 rw init=/bin/bash"

BESIPPKG=y
BESIPPKG_LITE=y
BESIPPKG_HUGE=y
BESIPPKG_DEV=n

OWRT_IMG_DISK_NAME=openwrt-brcm2708-sdcard-vfat-squashfs.img
OWRT_IMG_KERNEL_NAME=openwrt-brcm2708-Image

BESIP_PACKAGES += zabbix-agentd=y

OWRT_CONFIG_SET += TARGET_brcm2708=y \
	TARGET_brcm2708_RaspberryPi=y \
	BRCM2708_SD_BOOT_PARTSIZE=20 \
	TARGET_ROOTFS_PARTSIZE=300 \
	TARGET_ROOTFS_SQUASHFS=y
