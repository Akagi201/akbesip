# This target will generate image for router wr740nv4
# No besip packages will be inside
# It will act only as zabbix proxy

# CPU of box, settings for target
TARGET_CPU=ar71xx
OWRT_CONFIG_SET += TARGET_ar71xx=y TARGET_ar71xx_generic_TLWR841=y

OWRT_NAME=trunk
TARGET_NAME=zabbix_proxy-wr740n_$(BESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=mips
TARGET_QEMU_OPTS=-m64

BESIPPKG=n
BESIPPKG_LITE=n
BESIPPKG_HUGE=n
BESIPPKG_DEV=n

# Disable including of defaults for BESIP (this is not BESIP build but zabbix proxy build)
NOBESIP_DEFAULTS=packages uciprov

OWRT_IMG_BIN_NAME=openwrt-ar71xx-generic-tl-wr740n-v4-squashfs-sysupgrade.bin
OWRT_IMG_FACTORY_NAME=openwrt-ar71xx-generic-tl-wr740n-v4-squashfs-factory.bin

OWRT_IMG_PROFILE=TLWR841
OWRT_IMG_KERNEL_NAME=openwrt-$(TARGET_CPU)-generic-vmlinux.elf

OWRT_CONFIG_SET += UCIPROV_REBOOT_AFTER_CFG_APPLY=y UCIPROV_AUTOSTART=y UCIPROV_WAITFOROVERLAY="180" UCIPROV_SU=y UCIPROV_TGZ=y UCIPROV_USE_STATIC=y UCIPROV_USE_DNSSEC=y UCIPROV_USE_DNS=y UCIPROV_USE_DHCP=y \
 UCIPROV_TGZURI="{uri}/../files{fd}.tgz" \
 UCIPROV_SUURI="{uri}/../su{fd}.img" \
 UCIPROV_URI1="http://zabbixproxy.{d}/default/uci{fd}" UCIPROV_URI2="http://zabbixproxy.{d}/{ip}/uci{fd}" UCIPROV_URI3="http://zabbixproxy.{d}/{mac}/uci{fd}" \
 UCIPROV_RECOVERY=y

# Only this packages, no defaults or other besip packages
BESIP_PACKAGES = zabbix-proxy=y zabbix-agentd=y zabbix-extra-wifi=y zabbix-extra-network=y zabbix-extra-mac80211=y fping=y




