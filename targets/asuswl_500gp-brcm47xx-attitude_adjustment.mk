TARGET_CPU=brcm47xx
OWRT_NAME=attitude_adjustment
TARGET_NAME=asuswl500gp_$(BESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=mipsel
TARGET_QEMU_OPTS=-m 32

BESIPPKG=m
BESIPPKG_LITE=y
BESIPPKG_HUGE=m
BESIPPKG_DEV=m

OWRT_IMG_TRX_NAME=openwrt-$(TARGET_CPU)-squashfs.trx

BESIP_PACKAGES=kmod-usb-core=y kmod-usb-ohci=y kmod-usb2=y kmod-usb-storage=y gnugk=m

OWRT_CONFIG_SET += TARGET_brcm47xx=y TARGET_brcm47xx_WL500GPv1=y 


