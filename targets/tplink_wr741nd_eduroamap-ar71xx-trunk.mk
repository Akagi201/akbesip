TARGET_CPU=ar71xx
OWRT_NAME=trunk
TARGET_NAME=eduroamap_wr741nd_$(BESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=mips
TARGET_QEMU_OPTS=-m64

$(eval $(call BesipDefaults,eduroam))

OWRT_IMG_BIN_NAME=openwrt-ar71xx-generic-tl-wr741nd-v4-squashfs-sysupgrade.bin
OWRT_IMG_SYSUPGRADE_NAME=openwrt-ar71xx-generic-tl-wr741nd-v4-squashfs-sysupgrade.bin
OWRT_IMG_FACTORY_NAME=openwrt-ar71xx-generic-tl-wr741nd-v4-squashfs-factory.bin
OWRT_IMG_PROFILE=TLWR741
OWRT_IMG_KERNEL_NAME=openwrt-$(TARGET_CPU)-generic-vmlinux.elf
OWRT_CONFIG_SET += TARGET_ar71xx=y TARGET_ar71xx_generic_TLWR741=y


