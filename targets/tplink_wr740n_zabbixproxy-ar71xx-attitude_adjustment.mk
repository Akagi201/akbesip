# This target will generate image for router wr740nv4
# No besip packages will be inside
# It will act only as zabbix proxy

# CPU of box, settings for target
TARGET_CPU=ar71xx
OWRT_CONFIG_SET += TARGET_ar71xx=y TARGET_ar71xx_generic_TLWR740=y

# Openwrt name
OWRT_NAME=attitude_adjustment

# Target name
TARGET_NAME=zabbix_proxy-wr740n_$(BESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=mips
TARGET_QEMU_OPTS=-m64

BESIPPKG=n
BESIPPKG_LITE=n
BESIPPKG_HUGE=n
BESIPPKG_DEV=n

# Disable including of defaults for BESIP (this is not BESIP build but zabbix proxy build)
NOBESIP_DEFAULTS=packages uciprov

# Only this packages, no defaults or other besip packages
BESIP_PACKAGES = zabbix-proxy=y zabbix-agentd=y

OWRT_IMG_BIN_NAME=openwrt-ar71xx-generic-tl-wr740n-v4-squashfs-sysupgrade.bin
OWRT_IMG_FACTORY_NAME=openwrt-ar71xx-generic-tl-wr740n-v4-squashfs-factory.bin
OWRT_IMG_PROFILE=TLWR740NV4
OWRT_IMG_KERNEL_NAME=openwrt-$(TARGET_CPU)-generic-vmlinux.elf

