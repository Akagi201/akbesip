TARGET_CPU=x86
OWRT_NAME=trunk
TARGET_NAME=virtual_$(BESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=i386
TARGET_QEMU_OPTS=-m 512

OWRT_IMG_DISK_NAME=openwrt-$(TARGET_CPU)-generic-combined-squashfs.img
OWRT_IMG_KERNEL_NAME=openwrt-$(TARGET_CPU)-generic-vmlinuz
OWRT_IMG_ISO_NAME=openwrt-$(TARGET_CPU)-generic.iso

BESIPPKG=y
BESIPPKG_LITE=y
BESIPPKG_HUGE=y
BESIPPKG_DEV=m

BESIP_PACKAGES= \
      kmod-usb-ohci=y kmod-usb-uhci=y kmod-usb2=y \
      kmod-usb-net=y \
      kmod-ata-core=y kmod-libsas=y kmod-loop=y kmod-ide-core=y kmod-ata-nvidia-sata=y kmod-ata-piix=y kmod-ata-via-sata=y kmod-ata-ahci=y block-mount=y \
      valgrind=m bash-completion=y bluez-utils=y swap-utils=y fdisk=y e2fsprogs=y resize2fs=y tune2fs=y kmod-fs-btrfs=y btrfs-progs=y kmod-fs-ext4=y kmod-crypto-core=y

OWRT_CONFIG_SET += \
      TOOLCHAINOPTS=y SSP_SUPPORT=y TLS_SUPPORT=y \
      MC_EDITOR=y MC_DIFF_VIEWER=y \
      TARGET_IMAGES_PAD=y \
      TARGET_KERNEL_PARTSIZE=100 TARGET_ROOTFS_PARTNAME=\"/dev/sda2\" TARGET_ROOTFS_PARTSIZE=150 TARGET_ROOTFS_MAXINODE=60000 \
      EXTROOT_SETTLETIME=2 \
      TARGET_ROOTFS_ISO=y \
      USE_EGLIBC=y INSIGHT=y

OWRT_CONFIG_UNSET += MC_DISABLE_VFS

OWRT_ADDED_FILES += fstab.virtual:/etc/config/fstab
OWRT_ADDED_ETCDEFAULT += network_dhcp_eth0.uci:/etc/config/network

EMBEDED_MODULES += SATA_AHCI SATA_AHCI_PLATFORM SATA_INIC162X SATA_ACARD_AHCI SATA_SIL24 ATA_SFF \
		    PARAVIRT_GUEST KVM_GUEST KVM_CLOCK PARAVIRT_TIME_ACCOUNTING VMWARE_BALLOON NETCONSOLE VMXNET3 VIRTIO_PCI VIRTIO_BALLOON VIRTIO_MMIO ACPI VIRTIO_BLK PARAVIRT \
		    NET_9P NET_9P_VIRTIO 9P_FS 9P_FS_POSIX_ACL

