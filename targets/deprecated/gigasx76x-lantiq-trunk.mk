TARGET_CPU=lantiq
OWRT_NAME=trunk
TARGET_NAME=GIGASX76X_$(BESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=mips
TARGET_QEMU_OPTS=-m64

BESIPPKG=m
BESIPPKG_LITE=y
BESIPPKG_HUGE=m
BESIPPKG_DEV=m

#OWRT_FORCE_SVN_REV=-r34685
#OWRT_FORCE_PACKAGES_SVN_REV=-r34708

OWRT_IMG_BIN_NAME=openwrt-lantiq-danube-GIGASX76X-squashfs.image 
OWRT_IMG_KERNEL_NAME=openwrt-lantiq-danube-GIGASX76X-uImage

BESIP_PACKAGES= kmod-usb-core=y kmod-gigaset=y uboot-lantiq-gigasx76x_nor=y uboot-lantiq-gigasx76x_ram=y

OWRT_CONFIG_SET += TARGET_lantiq=y TARGET_lantiq_danube=y TARGET_lantiq_danube_GIGASX76X=y 
