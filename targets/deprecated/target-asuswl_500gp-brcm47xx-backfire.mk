TARGET_CPU=brcm47xx
OWRT_NAME=backfire
TARGET_NAME=asuswl500gp_$(BESIP_VERSION)-$(OWRT_NAME)
OWRT_PACKAGES=10.03.2

TARGET_QEMU=mipsel
TARGET_QEMU_OPTS=-m64

BESIPPKG=m
BESIPPKG_LITE=y
BESIPPKG_HUGE=m
BESIPPKG_DEV=m

OWRT_IMG_TRX_NAME=openwrt-$(TARGET_CPU)-squashfs.trx

BESIP_PACKAGES += kmod-usb-core=y kmod-usb-core=y kmod-usb-ohci=y kmod-usb2=y kmod-usb-storage=y

OWRT_CONFIG_SET += TARGET_brcm47xx=y TARGET_brcm47xx_WL500GPv1=y 
