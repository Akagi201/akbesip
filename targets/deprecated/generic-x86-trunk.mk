TARGET_CPU=x86
OWRT_NAME=trunk
TARGET_NAME=generic_$(BESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=i386
TARGET_QEMU_OPTS=-m 512

OWRT_IMG_DISK_NAME=openwrt-$(TARGET_CPU)-generic-combined-squashfs.img
OWRT_IMG_KERNEL_NAME=openwrt-$(TARGET_CPU)-generic-vmlinuz
OWRT_IMG_ISO_NAME=openwrt-$(TARGET_CPU)-generic.iso
OWRT_IMG_SQUASHFS_NAME=openwrt-x86-generic-rootfs-squashfs.img

BESIPPKG=y
BESIPPKG_LITE=y
BESIPPKG_HUGE=y
BESIPPKG_DEV=y

BESIP_PACKAGES= \
	kmod-ata-core=y kmod-ata-ahci=y block-mount=y \
	valgrind=m bash-completion=y swap-utils=y fdisk=y e2fsprogs=y resize2fs=y tune2fs=y kmod-fs-ext4=y
