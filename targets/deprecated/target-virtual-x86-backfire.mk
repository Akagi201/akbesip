TARGET_CPU=x86
OWRT_NAME=backfire
OWRT_PACKAGES=10.03.2
TARGET_NAME=virtual_$(BESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=i386
TARGET_QEMU_OPTS=-m 512

OWRT_IMG_DISK_NAME=openwrt-$(TARGET_CPU)-generic-combined-squashfs.img

BESIPPKG=y
BESIPPKG_LITE=y
BESIPPKG_HUGE=y
BESIPPKG_DEV=m

BESIP_PACKAGES= \
      kmod-usb-net=y kmod-usb-core=y \
      kmod-libsas=y kmod-loop=y kmod-ide-core=y kmod-ata-nvidia-sata=y kmod-ata-piix=y kmod-ata-via-sata=y kmod-ata-ahci=y block-mount=y \
      bash-completion=y bluez-utils=y swap-utils=y fdisk=y e2fsprogs=y resize2fs=y tune2fs=y kmod-fs-btrfs=y btrfs-progs=y kmod-fs-ext4=y

OWRT_CONFIG_SET += TOOLCHAINOPTS=y TLS_SUPPORT=y \
    MC_EDITOR=y MC_DIFF_VIEWER=y \
    TARGET_ROOTFS_PARTNAME=\"/dev/sda2\" \
    EXTROOT_SETTLETIME=2

OWRT_CONFIG_UNSET += MC_DISABLE_VFS
