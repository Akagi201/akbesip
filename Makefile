.SUFFIXES:

include inc/config.mk
include inc/lib.mk
include inc/version.inc


# Load default files to copy
$(eval $(call BesipDefaults,files))

# We can define CFG in local.mk
-include inc/local.mk

ifneq ($(TARGET),)
  CFG=targets/$(TARGET).mk
endif

# If CFG was specified, include CFG file, else x86-besip.mk
ifneq ($(CFG),)
-include $(CFG)
else
include targets/virtual-x86-trunk.mk
endif

include inc/postconfig.mk

ifeq ($(PKG),)
PKG=besip-huge
endif

all: info prereq download-owrt switch-owrt prepare-kernel besip-images

quick: info asterisk kamailio3 besip-packages
	make -C $(OWRT_DIR) target/linux/install

info:
	@echo "\n============== Building BESIP $(TARGET_NAME) for architecture $(TARGET_CPU) "
	@echo "BESIP revision: $(BESIP_GIT_REV)"
	@echo "BESIP verion: $(BESIP_VERSION)"
	@echo "OpenWrt name: $(OWRT_NAME)"
	@echo "OpenWrt revision: $(OWRT_GIT_REV)"
	@echo "OpenWrt build dir: $(OWRT_DIR)"
	@echo "OpenWrt force revision: $(OWRT_FORCE_GIT_REV)"
	@echo "OpenWrt force packages revision: $(OWRT_FORCE_PACKAGES_GIT_REV)"
	@echo "OpenWrt GIT url: $(OWRT_GIT)"
	@echo 'OpenWrt feeds dir: $(OWRT_FEEDS_PKG),$(OWRT_FEEDS_LUCI),$(OWRT_FEEDS_ROUTING),$(OWRT_FEEDS_TELEPHONY)'
	@echo "Openwrt images: $(OWRT_IMAGES)"
	@echo "Target images will be: $(BESIP_IMAGES)"
	@echo "Default settings: $(BESIP_DEFAULTS)"
	@echo "No default settings: $(BESIP_NODEFAULTS)"
ifneq ($(VERBOSE),)
	@echo "OpenWrt packages: '$(BESIP_PACKAGES)'"	
	@echo "OpenWrt added files: '$(OWRT_ADDED_FILES)'"
endif
	@echo "==============\n\n"
	@echo

info2:  info
	@echo "Linux dir: $$($(OWRT_DIR)/scripts/shell.sh env LINUX_DIR)"
	@echo "Linux config: $$($(OWRT_DIR)/scripts/shell.sh env LINUX_DIR)/.config"
	@echo "Linux target config: $$($(OWRT_DIR)/scripts/shell.sh env LINUX_TARGET_CONFIG)"
	@echo "Target dir: $(BESIP_IMG_DIR)"
	@echo "Log directory: $(LOG_DIR)"
	@echo "Stamp directory: $(STAMP_DIR)"
	@echo "Make arguments: $(MAKE_ARGS)"
	@echo "OpenWrt config set: '$(OWRT_CONFIG_SET)'"
	@echo "OpenWrt config unset: '$(OWRT_CONFIG_UNSET)'"
	@echo "Embedded modules: '$(EMBEDED_MODULES)'"
	@echo "Dependencies: $(BESIP_DEPENDS)"
	@echo "--------------"
	@echo


env-info:
	@echo "BESIP_GIT_REV=$(BESIP_GIT_REV)"
	@echo "BESIP_VERSION=$(BESIP_VERSION)"
	@echo "OWRT_DIR=$(OWRT_DIR)"
	@echo "BESIP_IMG_DIR=$(BESIP_IMG_DIR)"
	@echo "MAKE=$(MAKE) CFG=$(CFG)"
	@echo "OMAKE=$(BMAKE) -C $(OWRT_DIR)"
	@echo "OWRT_ADDED_FILES='$(OWRT_ADDED_FILES)'"
	
prereq-i686:
	@#for i in $(DEPS_x86); do if ! dpkg -l $$i >/dev/null; then echo "Missing $$i package! Run sudo apt-get install $$i"; exit 1; fi; done
prereq-x86_64:
	@#for i in $(DEPS_x86_64); do if ! dpkg -l $$i >/dev/null; then echo "Missing $$i package! Run sudo apt-get install $$i"; exit 1; fi; done
prereq: $(SPREREQ) prereq-$(ARCH)
$(SPREREQ): 
	$(call mkdirall)
	if [ -f $(OWRT_DIR)/.config ]; then \
	  $(BMAKE) -C $(OWRT_DIR) prereq; \
	  touch $(SPREREQ); \
	fi

download-owrt: git-clone-owrt
git-clone-owrt: $(SGITCLONE)
$(SGITCLONE): $(SPREREQ)
	if ! [ -f $(OWRT_DIR)/Makefile ]; then git clone $(OWRT_GIT) $(OWRT_DIR); fi
	cd $(OWRT_DIR) && git pull ; git checkout $(OWRT_FORCE_GIT_REV) .
	$(call mkdirall)
	touch $(SGITCLONE)

update-feeds: $(SFEEDSU)
$(SFEEDSU): download-owrt
	echo $(OWRT_FEEDS_PKG) >$(OWRT_DIR)/feeds.conf
	echo $(OWRT_FEEDS_LUCI) >>$(OWRT_DIR)/feeds.conf
	echo $(OWRT_FEEDS_ROUTING) >>$(OWRT_DIR)/feeds.conf
	echo $(OWRT_FEEDS_TELEPHONY) >>$(OWRT_DIR)/feeds.conf
	echo $(OWRT_FEEDS_ADDONS1) >>$(OWRT_DIR)/feeds.conf
	echo $(OWRT_FEEDS_ADDONS2) >>$(OWRT_DIR)/feeds.conf
	echo 
	echo src-link besip $(PWD)/packages >>$(OWRT_DIR)/feeds.conf
	echo src-link besip_$(OWRT_NAME) $(PWD)/packages-$(OWRT_NAME) >>$(OWRT_DIR)/feeds.conf
ifeq ($(BESIP_VERSION),trunk)
	echo src-link besip_incomplete $(PWD)/packages-incomplete >>$(OWRT_DIR)/feeds.conf
endif
	cd $(OWRT_DIR); ./scripts/feeds update -a
	touch $(SFEEDSU)

clean: clean-besip clean-owrt gitclean clean-kvm
	@if [ -n "$(BESIP_IMG_DIR)" ]; then $(RM) -rf $(BESIP_IMG_DIR); fi
	@if [ -n "$(BESIP_PACKAGES_DIR)" ];then $(RM) -rf $(BESIP_PACKAGES_DIR); fi
	@if [ -n "$(LOG_DIR)" ];then $(RM) -rf $(LOG_DIR); fi
	@if [ -d "$(OWRT_DIR)" ]; then $(BMAKE) -C $(OWRT_DIR) -j$(JOBS) defconfig package/cleanup package/base-files/clean; fi
	$(RM) -f $(SOWRTCFG)

clean-owrt:
	@make -C $(OWRT_DIR) clean -j4
	
clean-stamp:
	$(RM) -f $(STAMP_DIR)/* $(OSTAMP_DIR)/*; true

clean-image:
	$(RM) -f $(BESIP_IMG_DIR)/* $(OWRT_IMG) $(OWRT_IMG_SQUASHFS) $(SOWRTIMG); true

clean-feeds:
	cd $(OWRT_DIR); ./scripts/feeds clean
	$(RM) -f $(SFEEDSU) $(OWRT_DIR)/feeds.conf $(OWRT_DIR)/feeds/besip

clean-besip:
	if [ -d $(OWRT_DIR)/feeds/besip/net/besip ]; then $(BMAKE) -C $(OWRT_DIR)/feeds/besip/net/besip clean TOPDIR=$(OWRT_DIR); fi
	$(RM) -f $(SOWRTIMG) $(BESIP_IMAGES)

distclean:
	$(RM) -rf $(OWRT_DIR) $(BUILD_DIR)/* $(BUILD_DIR)/dl $(STAMP_DIR)/*
	
dirclean:
	$(BMAKE) -C $(OWRT_DIR) defconfig dirclean -j$(JOBS)
	$(BMAKE) gitclean

gitclean:
	if [ -d "$(OWRT_DIR)" ]; then \
	  cd $(OWRT_DIR) && git reset --hard FETCH_HEAD && git clean -f -d; \
	 for i in $(OWRT_DIR)/feeds/*; do \
	   if [ -d $$i/.svn ]; then svn revert -R $$i; fi; \
	   if [ -d $$i/.git ]; then (cd $$i; git reset --hard FETCH_HEAD && git clean -f -d ); fi; \
	 done; \
	 $(RM) -f $(OSTAMP_DIR)/*; \
	fi

update: update-cleanstamp unpatch-owrt download-owrt update-feeds update-us patch-owrt config-owrt
update-cleanstamp:
	$(RM) -f $(SFEEDSU) $(SFEEDSI) update-owrt $(SOWRTCFG)

update-us:
	#svn update

changes:
	@git status

force-install-feeds:
	$(RM) -f $(SFEEDSI)
	$(BMAKE) install-feeds
	
install-feeds: $(SFEEDSI)
$(SFEEDSI):
	#cd $(OWRT_DIR); for i in $(shell echo $(BESIP_PACKAGES) $(BESIP_DEPENDS) |tr -d '+' |tr ' ' '\n'|cut -d '=' -f 1); do ./scripts/feeds install $$i; done
	cd $(OWRT_DIR); ./scripts/feeds install -a
	touch $(SFEEDSI)
	$(RM) -f $(SOWRTCFG)

uninstall-feeds:
	cd $(OWRT_DIR); ./scripts/feeds uninstall -a
	$(RM) -f $(SFEEDSI) $(SOWRTCFG)

update-owrt: download-owrt
$(SGITPULL):
	cd $(OWRT_DIR) && git pull ; git checkout $(OWRT_FORCE_GIT_REV) .
	touch $(SGITPULL)
	$(RM) -f $(SOWRTCFG)

test-dotconf:
	if ! [ -f "$(OWRT_DIR)/.config" ]; then $(RM) -f $(SOWRTCFG); fi
	
config-owrt: test-dotconf $(SOWRTCFG) 
$(SOWRTCFG):
	@$(BMAKE) install-feeds
	@cd $(OWRT_DIR); \
	if [ -n "$(TARGET_CFG)" ]; then \
		grep -v -E '$(shell echo $(COMPILE_PACKAGES) $(BESIP_PACKAGES) $(OWRT_CONFIG_SET)|sed "s/\(.*\)\=/\1\=/g")\>\|some_nunusual_text' $(TARGET_CFG) >$(OWRT_DIR)/.config-tmp1; \
	else \
		$(RM) -f $(OWRT_DIR)/.config; \
		touch $(OWRT_DIR)/.config-tmp1; \
	fi
	@[ -n "$(VERBOSE)" ] && echo -n "ENABLING packages: "; true
	@for i in $(COMPILE_PACKAGES) $(BESIP_PACKAGES); do \
		[ -n "$(VERBOSE)" ] && echo -n "CONFIG_PACKAGE_$$i, " >&2; \
		echo CONFIG_PACKAGE_$$i ; \
	done <$(OWRT_DIR)/.config-tmp1 >$(OWRT_DIR)/.config-tmp2;
ifneq ($(BESIPPKG_LITE),n)
	@(for i in $(BESIP_LITE_DEPS); do echo CONFIG_PACKAGE_$$(echo $$i |tr -d '+')=$(BESIPPKG_LITE); done) >>$(OWRT_DIR)/.config-tmp2
endif
ifneq ($(BESIPPKG),n)
	@(for i in $(BESIP_DEPS); do echo CONFIG_PACKAGE_$$(echo $$i |tr -d '+')=$(BESIPPKG) ; done | grep -v CONFIG_PACKAGE_besip) >>$(OWRT_DIR)/.config-tmp2
endif
ifneq ($(BESIPPKG_HUGE),n)
	@(for i in $(BESIP_HUGE_DEPS); do echo CONFIG_PACKAGE_$$(echo $$i |tr -d '+')=$(BESIPPKG_HUGE); done | grep -v CONFIG_PACKAGE_besip) >>$(OWRT_DIR)/.config-tmp2
endif
ifneq ($(BESIPPKG_DEV),n)
	@(for i in $(BESIP_DEV_DEPS); do echo CONFIG_PACKAGE_$$(echo $$i |tr -d '+')=$(BESIPPKG_DEV); done | grep -v CONFIG_PACKAGE_besip) >>$(OWRT_DIR)/.config-tmp2
endif
	@[ -n "$(VERBOSE)" ] && echo -n "ENABLING configs: "; true
	@for i in $(OWRT_CONFIG_SET); do \
		[ -n "$(VERBOSE)" ] && echo -n "CONFIG_$$i, " >&2; \
		echo CONFIG_$$i; \
	done >>$(OWRT_DIR)/.config-tmp2;
	@$(CP) $(OWRT_DIR)/.config-tmp2 $(OWRT_DIR)/.config
	@$(BMAKE) -C $(OWRT_DIR) defconfig >/dev/null
	@cat $(OWRT_DIR)/.config | grep -vE '$(shell echo $(OWRT_CONFIG_UNSET)|sed "s/\ /\(\>\|\=\|\-\)\|/g")|some_unusual_text' >$(OWRT_DIR)/.config-tmp3; true
	@for i in $(OWRT_CONFIG_UNSET); do echo "# CONFIG_$$i is not set"; done >>$(OWRT_DIR)/.config-tmp3
	@$(CP)  $(OWRT_DIR)/.config-tmp3  $(OWRT_DIR)/.config
	@for i in $(filter %=y,$(BESIP_PACKAGES)); do if ! grep -q $$i $(OWRT_DIR)/.config; then echo Missing package $$i, look into feeds and dependencies; exit 2; fi; done
	@for i in $(filter %=m,$(BESIP_PACKAGES)); do if ! grep -q $$i $(OWRT_DIR)/.config; then echo Missing package $$i, look into feeds and dependencies; exit 2; fi; done
	@for i in $(OWRT_CONFIG_SET); do echo $$i; done | sort | uniq -d | while read line; do echo Duplicate config set $$line; exit 2; done
	@for i in $(OWRT_CONFIG_SET); do if ! grep -q $$i $(OWRT_DIR)/.config; then echo Missing config option $$i, problem with config; exit 2; fi; done
	@for i in $(OWRT_CONFIG_UNSET); do if grep -q $$i $(OWRT_DIR)/.config | grep -qv '^#'; then echo Disabled package $$i inside! Problem with config.; exit 2; fi; done
	@if [ -d $(OWRT_DIR)/files/ ]; then $(RM) -rf $(OWRT_DIR)/files; fi
	@$(call AddFiles)
	@echo OWRT_IMG_SYSUPGRADE_NAME=$(OWRT_IMG_SYSUPGRADE_NAME) >$(OWRT_DIR)/tmp/besip-inc.mk
	@echo BESIP_PACKAGES=$(BESIP_PACKAGES) >>$(OWRT_DIR)/tmp/besip-inc.mk
	@echo EMBEDED_MODULES=$(EMBEDED_MODULES) >>$(OWRT_DIR)/tmp/besip-inc.mk
	@echo OWRT_ADDED_ETCDEFAULT=$(OWRT_ADDED_ETCDEFAULT) >>$(OWRT_DIR)/tmp/besip-inc.mk
	@#touch $(SOWRTCFG)

force-config-owrt:
	$(RM) -f $(SOWRTCFG)
	@$(BMAKE) config-owrt
	
switch-owrt: download-owrt
	$(call mkdirall)
	@rm -rf $(OWRT_DIR)/files
	@if $(call owrt_env,list) | grep -q "$(TARGET_NAME)"; then \
	  echo n | $(call owrt_env,switch "$(TARGET_NAME)"); \
	else \
	  echo Y | $(call owrt_env,new "$(TARGET_NAME)") ; \
	fi

prepare-owrt: switch-owrt $(SOWRTPREP)
$(SOWRTPREP): $(SPATCHED) $(SFEEDSI) $(SOWRTCFG) $(OWRTTCPREP) $(SKERNPREP)
	touch $(SOWRTPREP)

prepare-toolchain: $(OWRTTCPREP)
$(OWRTTCPREP):
	$(call owrt_testconf)
	@$(BMAKE) -C $(OWRT_DIR) prepare -j$(JOBS)
	touch $(OWRTTCPREP)

config-kernel: prepare-kernel
prepare-kernel: $(SKERNPREP)
$(SKERNPREP): $(PATCHED) $(SOWRTCFG)
	$(call mkdirall)
	$(call owrt_testconf)
	if [ -z "$$($(OWRT_DIR)/scripts/shell.sh env LINUX_DIR)" ]; then echo "Problem with openwrt patch ??" ;exit 1; fi
	@$(BMAKE) -C $(OWRT_DIR) tools/install -j$(JOBS)
	@$(BMAKE) -C $(OWRT_DIR)  target/prereq -j$(JOBS)
	@$(BMAKE) -C $(OWRT_DIR) target/linux/prepare -j$(JOBS)
	@for i in $(EMBEDED_MODULES); do if ! grep -q "CONFIG_$$i\=y" $$($(OWRT_DIR)/scripts/shell.sh env LINUX_DIR)/.config; then echo "Missing embeded module $$i - possible kernel conf problem"; exit 1; fi; done
	touch $(SKERNPREP)
	
tools:
	$(call owrt_testconf)
	$(MAKE) -C $(OWRT_DIR) tools/install -j$(JOBS)

patch-owrt: $(SPATCHED)
$(SPATCHED): $(SFEEDSU)
	cd $(OWRT_DIR); sh $(PWD)/apply.sh "$(PWD)" "$(OSTAMP_DIR)" patch-$(OWRT_NAME)
	cd $(OWRT_DIR); sh $(PWD)/apply.sh "$(PWD)" "$(OSTAMP_DIR)" patch $(OWRT_PATCHES)
	chmod +x $(OWRT_DIR)/scripts/getvar.sh $(OWRT_DIR)/scripts/shell.sh
	touch $(SPATCHED)
	$(RM) -f $(SOWRTCFG)

unpatch-owrt: gitclean

owrt-image: $(OWRT_IMAGES)
$(OWRT_IMAGES): $(SOWRTIMG)
	if [ -f "$@.gz" ] && ! [ -f "$@" ]; then gunzip <"$@.gz" >"$@"; fi
	if [ -f "$@" ] && ! [ -f "$@.gz" ]; then gzip <"$@" >"$@.gz"; fi

$(SOWRTIMG):
	$(call mkdirall)
	$(call owrt_testconf)
	@for i in $(EMBEDED_MODULES); do if ! grep -q "CONFIG_$$i\=y" $$($(OWRT_DIR)/scripts/shell.sh env LINUX_DIR)/.config; then echo "Missing embeded module $$i - possible kernel conf problem"; exit 1; fi; done
ifeq ($(VERBOSE),)
	$(BMAKE) -C $(OWRT_DIR) IGNORE_ERRORS=y -j$(JOBS) || $(BMAKE) -C $(OWRT_DIR) V=99 -j1
else
	$(BMAKE) -C $(OWRT_DIR) V=99 -j1
endif
	touch $(SOWRTIMG)

download: download-owrt config-owrt
	$(call mkdirall)
	$(BMAKE) -C $(OWRT_DIR) download -j$(JOBS)
	
ifneq ($(BESIP_IMG_SQUASHFS),)
squashfs-image: $(BESIP_IMG_SQUASHFS)
$(BESIP_IMG_SQUASHFS): $(OWRT_IMG_SQUASHFS)
	$(call mkdirall)
	$(CP) $(OWRT_IMG_SQUASHFS) $(BESIP_IMG_SQUASHFS)
	md5sum $(BESIP_IMG_SQUASHFS) >$(BESIP_IMG_SQUASHFS).md5sum
endif

ifneq ($(OWRT_IMG_VMDK),)
vmdk-image: $(BESIP_IMG_VMDK)
$(BESIP_IMG_VMDK): $(OWRT_IMG_VMDK) vmx-cfg kvm-sh
	$(call mkdirall)
	$(CP) $(OWRT_IMG_VMDK) $(BESIP_IMG_VMDK)
	md5sum $(BESIP_IMG_VMDK) >$(BESIP_IMG_VMDK).md5sum
else
vmdk-image: $(BESIP_IMG_VMDK)
$(BESIP_IMG_VMDK): $(OWRT_IMG_DISK) vmx-cfg
	$(call mkdirall)
	qemu-img convert -f raw $(OWRT_IMG_DISK) -O vmdk $(BESIP_IMG_VMDK)
	md5sum $(BESIP_IMG_VMDK) >$(BESIP_IMG_VMDK).md5sum
endif

vmx-cfg:
	qemu-img create -f vmdk $(BESIP_DATA_VMDK) 1G
	md5sum $(BESIP_DATA_VMDK) >$(BESIP_DATA_VMDK).md5sum
	cat $(PWD)/files/besip.vmx | \
		sed "s/BESIP_IMG_VMDK/$(shell basename $(BESIP_IMG_VMDK))/" | \
		sed "s/BESIP_DATA_VMDK/$(shell basename $(BESIP_DATA_VMDK))/" \
		>$(BESIP_VMX)
	md5sum $(BESIP_VMX) >$(BESIP_VMX).md5sum

kvm-sh:
	@echo '#!/bin/sh' >$(BESIP_KVMSH)
	@echo 'vmdk=$$(dirname $$0)/$(BESIP_IMG_PREFIX).vmdk' >>$(BESIP_KVMSH)
	@echo 'data=$$(dirname $$0)/$(shell basename $(BESIP_DATA_VMDK))' >>$(BESIP_KVMSH)
	@echo 'if [ -n "$$1" ] ; then vmdk="$$1"; fi' >>$(BESIP_KVMSH)
	@echo 'if [ -n "$$2" ]; then data="$$2"; fi' >>$(BESIP_KVMSH)
	@echo 'if [ -n "$$data" ]; then ddisc="-drive file=$$data,index=1,media=disk,if=virtio"; fi' >>$(BESIP_KVMSH)
	@echo '$(QEMU) -nographic \
		-device ahci,id=ahci0 \
		-drive file=$$vmdk,index=0,media=disk,if=none,id=drive-sata0-0-0 \
		-device ide-drive,bus=ahci0.0,drive=drive-sata0-0-0,id=sata0-0-0 \
		-net nic,model=e1000 -net nic,model=e1000 \
		$$ddsic' \
		>>$(BESIP_KVMSH)
	
$(IMG_DIR)/$(IB_NAME).tar.bz2:
	$(BMAKE) -C $(OWRT_DIR) target/imagebuilder/install
ib imagebuilder: $(SOWRTIMGB)
ifneq ($(OWRT_IMG_PROFILE),)
	rm -rf $(OWRT_DIR)/tmp/$(IB_NAME)/packages
	ln -s $(OWRT_PKG_DIR) $(OWRT_DIR)/tmp/$(IB_NAME)/packages
	rm -rf $(OWRT_DIR)/tmp/$(IB_NAME)/bin
	ln -s $(OWRT_DIR)/bin $(OWRT_DIR)/tmp/$(IB_NAME)/bin
	rm -f $(OWRT_PKG_DIR)/Packages*
	mkdir -p  $(OWRT_DIR)/files
	$(call AddFiles)
	make -C $(OWRT_DIR)/tmp/$(IB_NAME) image \
	    PROFILE=$(OWRT_IMG_PROFILE) \
	    PACKAGES="$(BESIP_EPACKAGES)" \
	    FILES="$(OWRT_DIR)/files"
endif

ibclean:
	rm -rf $(OWRT_DIR)/tmp/$(IB_NAME) $(IMG_DIR)/$(IB_NAME).tar.bz2

$(SOWRTIMGB): $(IMG_DIR)/$(IB_NAME).tar.bz2
ifneq ($(OWRT_IMG_PROFILE),)
	cd $(OWRT_DIR)/tmp && tar -jxf $(IMG_DIR)/$(IB_NAME).tar.bz2
	touch $(SOWRTIMGB)
endif

fastimg: force-config-owrt besip-packages ib

clean-ib clean-imagebuilder:
	rm -f $(SOWRTIMGB) $(IMG_DIR)/$(IB_NAME).tar.bz2

ifneq ($(BESIP_IMG_BIN),)
$(OWRT_IMG_BIN): imagebuilder
bin-image: $(BESIP_IMG_BIN)
$(BESIP_IMG_BIN): $(OWRT_IMG_BIN)
	$(call mkdirall)
	$(CP) $(OWRT_IMG_BIN) $(BESIP_IMG_BIN) 
	md5sum $(BESIP_IMG_BIN) >$(BESIP_IMG_BIN).md5sum
endif

ifneq ($(BESIP_IMG_FACTORY),)
$(OWRT_IMG_FACTORY): imagebuilder
trx-image: $(BESIP_IMG_FACTORY)
$(BESIP_IMG_FACTORY): $(OWRT_IMG_FACTORY)
	$(call mkdirall)
	$(CP) $(OWRT_IMG_FACTORY) $(BESIP_IMG_FACTORY) 
	md5sum $(BESIP_IMG_FACTORY) >$(BESIP_IMG_FACTORY).md5sum	
endif

ifneq ($(BESIP_IMG_DISK),)
disk-image: $(BESIP_IMG_DISK)
$(BESIP_IMG_DISK): $(OWRT_IMG_DISK)
	$(call mkdirall)
	if [ -f $(OWRT_IMG_DISK).gz ] && ! [ -f $(OWRT_IMG_DISK) ]; then \
		zcat $(OWRT_IMG_DISK).gz >$(OWRT_IMG_DISK) ; \
	fi
	$(CP) $(OWRT_IMG_DISK) $(BESIP_IMG_DISK)
	md5sum $(BESIP_IMG_DISK) >$(BESIP_IMG_DISK).md5sum	
endif

ifneq ($(BESIP_IMG_KERNEL),)
kernel-image: $(BESIP_IMG_KERNEL)
$(BESIP_IMG_KERNEL): $(OWRT_IMG_KERNEL)
	$(call mkdirall)
	$(CP) $(OWRT_IMG_KERNEL) $(BESIP_IMG_KERNEL)
	md5sum $(BESIP_IMG_KERNEL) >$(BESIP_IMG_KERNEL).md5sum	
endif


ifneq ($(BESIP_IMG_SYSUPGRADE),)
sysupgrade-image: $(BESIP_IMG_SYSUPGRADE)
$(BESIP_IMG_SYSUPGRADE): $(BESIP_IMG_KERNEL) $(BESIP_IMG_SQUASHFS) $(BESIP_IMG_DISK)
	$(call mkdirall)
	$(CP) $(OWRT_IMG_SYSUPGRADE) $(BESIP_IMG_SYSUPGRADE)
	md5sum $(BESIP_IMG_SYSUPGRADE) >$(BESIP_IMG_SYSUPGRADE).md5sum
endif


ifneq ($(BESIP_IMG_ISO),)
iso-image: $(BESIP_IMG_ISO)
$(BESIP_IMG_ISO): $(OWRT_IMG_ISO)
	$(call mkdirall)
	$(CP) $(OWRT_IMG_ISO) $(BESIP_IMG_ISO)
	md5sum $(BESIP_IMG_ISO) >$(BESIP_IMG_ISO).md5sum
endif

besip-packages: 
	rm -f $(OWRT_DIR)/tmp/.package*
	$(BMAKE) -k -C $(OWRT_DIR) -j$(JOBS) $(foreach pkg,besip uciprov,package/$(pkg)/clean) -j3
	$(BMAKE) -k -C $(OWRT_DIR) -j$(JOBS) $(foreach pkg,besip uciprov,package/$(pkg)/install) -j3

besip-image: besip-images
besip-images: $(BESIP_IMAGES)
	@echo "Done! Images at $(BESIP_IMAGES)."

packages: config-owrt
	$(call owrt_testconf)
	@$(BMAKE) -C $(OWRT_DIR) package/compile package/install package/index -j$(JOBS) IGNORE_ERRORS=y

besip: $(SOWRTPREP)

kernel_menuconfig:
	$(BMAKE) -C $(OWRT_DIR) kernel_menuconfig 

kernel:
	$(BMAKE) -C $(OWRT_DIR) target/linux/compile -j$(JOBS)

clean-kernel:
	$(BMAKE) -C $(OWRT_DIR) target/linux/clean -j$(JOBS)

prov-tgz: $(KVM_TFTP_DIR)/files1.tgz
$(KVM_TFTP_DIR)/files1.tgz:
ifneq ($(KVM_TFTP_DIR),)
	mkdir -p $(KVM_TFTP_TMP)/etc
	echo src pkg file:///mnt/pkg >$(KVM_TFTP_TMP)/etc/opkg.conf
	echo dest root / >>$(KVM_TFTP_TMP)/etc/opkg.conf
	echo dest ram /tmp >>$(KVM_TFTP_TMP)/etc/opkg.conf
	echo lists_dir ext /var/opkg-lists >>$(KVM_TFTP_TMP)/etc/opkg.conf
	echo option overlay_root /overlay >>$(KVM_TFTP_TMP)/etc/opkg.conf
	mkdir -p $(KVM_TFTP_TMP)/etc/config
	cp files/fstab.dev $(KVM_TFTP_TMP)/etc/config/fstab
	mkdir -p $(KVM_TFTP_DIR) && cd $(KVM_TFTP_TMP) && tar -czf $(KVM_TFTP_DIR)/files1.tgz .
endif

test-kvm: prov-tgz
	@mkdir -p $(KVM_DIR)
	@if [ ! -f $(KVM_DIR)/disk2 ]; then dd "if=/dev/zero" "of=$(KVM_DIR)/disk2" "bs=1M" "count=256"; fi
	@mkdir -p $(KVM_DIR)/overlay $(KVM_DIR)/tmp $(KVM_DIR)/data
	$(KVM)	-nographic \
		-virtfs local,path=$(PWD),$(VIRTFS_SECURITY),mount_tag=src \
		-virtfs local,path=$(BESIP_IMG_DIR),$(VIRTFS_SECURITY),mount_tag=img \
		-virtfs local,path=$(OWRT_PKG_DIR),$(VIRTFS_SECURITY),mount_tag=pkg \
		-virtfs local,path=$(KVM_DIR)/tmp,$(VIRTFS_SECURITY),mount_tag=tmp \
		-device ahci,id=ahci0 \
		-drive file=$(BESIP_IMG_VMDK),index=0,media=disk,if=none,id=drive-sata0-0-0 \
		-device ide-drive,bus=ahci0.0,drive=drive-sata0-0-0,id=sata0-0-0 \
		-drive file=$(KVM_DIR)/disk2,index=1,media=disk,if=virtio

clean-kvm:
	@if [ -d "$(KVM_DIR)" ]; then rm -rf "$(KVM_DIR)"; fi

testdeps:
	@if [ -n "$(SUBPKG)" ]; then echo -n "\\nDependencies for $(SUBPKG): "; else echo -n "\\nDependencies for $(PKG):"; fi 
	@for p in $(shell echo $(BESIP_DEPENDS) | tr -d '+@'); do \
		if [ -n "$$p" ]; then \
			if ! $(BMAKE) -s testdeps PKG=$$p SUBPKG=$(SUBPKG)/$(PKG); then \
				exit 2; \
			fi; \
		fi; \
	done 2>/dev/null
	@if grep -q $(shell echo $(PKG) | tr -d '+@') $(OWRT_DIR)/.config; then \
		echo -n "$(PKG) OK, "; \
	else \
		echo \\n"$(PKG) missing!" ;\
		exit 2; \
	fi
	@if [ -z "$(SUBPKG)" ]; then echo; fi


