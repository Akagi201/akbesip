#!/bin/sh

. /usr/share/libubox/jshn.sh

if [ -n "$interface" ] && uciprov_protoenabled dhcp;
then
	boot_hook_add uciprov_geturi get_uri_dhcp
else
	interface=wan
fi

get_dhcp_var(){
	local var
	json_load $(ubus -S call "network.interface.$1" status) && \
	json_select data >/dev/null && \
	json_get_var var "lease_$2" && \
	echo $var
}

get_uri_dhcp(){
	local duri
	duri=$(get_dhcp_var $interface bootfile)
	setmacro uci_uris "$duri"
	setmacro dhcp_uri "$duri"
}

