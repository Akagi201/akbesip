#!/bin/sh

boot_hook_add uciprov_geturi get_uri_static_tgz
boot_hook_add uciprov_preapply uci_tgz
boot_hook_add uciprov_help uci_tgz_help

get_uri_static_tgz(){
	local tgzuri
	config_load uciprov
	config_get tgzuri "tgz" uri_static

	setmacro tgz_uri "$tgzuri"
}

uci_tgz(){
	local uri=$(uciprov_preparemodule tgz);
	if [ -n "$uri" ];
	then
		cd /
		fetch_uri "$uri" >/tmp/uciprov_tgz.tgz && \
		tar -zxvf /tmp/uciprov_tgz.tgz && \
		ssl_rehash && \
		uciprov_moduleok tgz $(md5sum /tmp/uciprov_tgz.tgz | cut -d ' ' -f 1) || \
		blog "Error untaring tgz!"
	fi
}

uci_tgz_help(){
	echo "Uciprov tar.gz files deployment module"
}

